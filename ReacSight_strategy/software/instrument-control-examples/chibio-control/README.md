
Both the `Flask` app and the instrument API are in a single file `app.py`.

This is the same `app.py` file as the original one (shipped with the Chi.Bio's), with some additions and modifications:

* OD measured with red filter to avoid contamination by blue light from optogenetic induction
* search for pump board de-activated for some reactors (need for only 3x8 = 24 pumps, i.e. 6 pump boards, so two reactors have no pump board attached)
* extra routes to customize control of stir, dilutions, etc...
