
For control of the Guava EasyCyte cytometer, the Flask interface and the python instrument control code are separated in the files `flaskr.py`, `guava_clicking.py`, and `guava_atlas.py`.

The `guava_clicking.py` module uses the python library `pyautogui` to automate cytometry acquisition via control of the instrument vendor GUI (software EasyCyte).

The `guava_atlas.py` module is a data management module to handle and store on our institutional network storage space all cytometry data.
