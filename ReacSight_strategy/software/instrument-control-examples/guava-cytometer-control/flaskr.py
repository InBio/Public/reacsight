from flask import Flask, request, redirect, url_for, render_template
import guava_clicking
import guava_atlas
from time import sleep
atlas_service = None

app = Flask(__name__)

@app.route('/')
def welcome():
    return 'Hello, this is the guava flask server'

@app.route('/prepare/<wells>/<int:n_events>')
def prepare_acquisition(wells,n_events):
    wells = wells.split('_')
    guava_clicking.prepare_acquisition(wells, n_events)
    sleep(8)
    return 'Done'

@app.route('/acquire')
def acquire():
    guava_clicking.acquire()
    return 'Started acquisition, done when data !'

@app.route('/toggle_tray')
def toggle_tray():
    guava_clicking.toggle_tray()
    sleep(12)
    return 'Done'

@app.route('/check_and_fetch_fcs3/{time_s}')
def check_and_fetch_fcs3(time_s):
    # to-do
    # probably need to rethink atlas service
    # maybe cut the problem in two: local backup first, then atlas backup
    # and use only local backup
    return 0


@app.route('/start_atlas_service')
def start_atlas_service():
    global atlas_service
    if atlas_service is not None and atlas_service.is_alive():
        return 'Atlas service already running'
    atlas_service = guava_atlas.AtlasService()
    atlas_service.start()
    return 'Atlas service started'
