For control of LED strips (for optogenetic), the Flask interface and the python instrument control code are in a single file `flaskr.py`.

The code presented here controls two arduinos, each of them controlling 7 LEDs for 8 reactors (56 LEDs in total).

The corresponding arduino code is also given in `arduino-code`.
