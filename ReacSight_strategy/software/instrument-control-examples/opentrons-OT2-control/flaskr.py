from flask import Flask, request, redirect, url_for, render_template
import opentrons as opentrons
import numpy as np
import json
import logging
import logging.handlers

logging.basicConfig(
        handlers=[logging.handlers.RotatingFileHandler('OT2.log', maxBytes=1*1024*1024, backupCount=10)],
        level=logging.INFO,
        format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
        datefmt='%Y-%m-%dT%H:%M:%S')

opentrons.robot.connect()
opentrons.robot.discover_modules()
opentrons.robot.reset()
opentrons.robot.home()



json_sampling = json.load(
    open(r'/usr/lib/python3.7/site-packages/opentrons/shared_data/labware/definitions/2/corning_96_wellplate_360ul_flat/1.json'))
json_sampling_metal = json.load(
    open(r'/usr/lib/python3.7/site-packages/opentrons/shared_data/labware/definitions/2/corning_96_wellplate_360ul_flat/1.json'))
json_waste_funnel = json.load(
    open(r'/usr/lib/python3.7/site-packages/opentrons/shared_data/labware/definitions/2/corning_96_wellplate_360ul_flat/1.json'))
json_cytoplate = json_sampling.copy()

#json_sampling["cornerOffsetFromSlot"] = {'x': 0, 'y': 2, 'z': 129}
json_sampling["cornerOffsetFromSlot"] = {'x': -4.5, 'y': 2, 'z': 129}
json_sampling_metal["cornerOffsetFromSlot"] = {'x': 116, 'y': 10, 'z': 129}
json_sampling_metal['parameters']['loadName'] = "Corning 96 Well Plate 360 µL Flat_metal"
json_waste_funnel["cornerOffsetFromSlot"] = {'x': 110, 'y': 10, 'z': 129}
json_waste_funnel['parameters']['loadName'] = "3D Printed trash"
json_cytoplate["cornerOffsetFromSlot"] = {'x': 125, 'y': 80, 'z': 62}

sampling = opentrons.labware.robot.add_container_by_definition(json_sampling,'7')
sampling_metal = opentrons.labware.robot.add_container_by_definition(json_sampling_metal,'7',share=True)
sampling_2 = opentrons.labware.robot.add_container_by_definition(json_sampling,'10')
sampling_metal_2 = opentrons.labware.robot.add_container_by_definition(json_sampling_metal,'10',share=True)

cytoplate = opentrons.labware.robot.add_container_by_definition(json_cytoplate,'6')
waste_funnel = opentrons.labware.robot.add_container_by_definition(json_waste_funnel,'3')
trough = opentrons.labware.load('trough-12row', '11')
tiprack = [opentrons.labware.load('opentrons-tiprack-300ul', slot)
           for slot in ['1', '4']]
labware_dictionary = {'sampling_metal':sampling_metal,'sampling':sampling,'sampling_metal_2':sampling_metal_2,'sampling_2':sampling_2,'cytoplate':cytoplate,'waste_funnel':waste_funnel,'trough':trough, 'tip_rack_1':tiprack[0], 'tip_rack_2':tiprack[1]}
pipette = opentrons.instruments.P300_Multi(mount='right', tip_racks=tiprack)

opentrons.robot.head_speed(x=240, y=160)

app = Flask(__name__) #this row has to be there

@app.route('/')
def nothin(): #does nothing
    return('all good buddy')

@app.route('/shakeit')
def shake():
    for i in range(3):
        pipette.move_to(waste_funnel['A1'])
        pipette.move_to(waste_funnel['A3'])
        pipette.move_to(waste_funnel['A1'])
    return('did some shaking dude')

@app.route('/tips/pick_up')
def tips_pick_up():
    pipette.pick_up_tip(presses = 1)
    return('tips picked')
@app.route('/tips/drop')
def tips_drop(): #should return tips in their original tiprack
    pipette.drop_tip()
    return('tips dropped')
@app.route('/tips/return')
def tips_return(): #drop tips in the bin
    pipette.return_tip()
    return('tips returned')
@app.route('/tips/reset')
def tips_reset(): # to do when refilling all tipracks
    pipette.reset()
    return('tips tracking reset')

@app.route('/blink/<int:number_of_blinks>') #blinks the blue button glow
def buttonglowblink(number_of_blinks):
    from time import sleep
    for t in range(number_of_blinks):
        opentrons.robot._driver.turn_on_red_button_light()
        sleep(0.2)
        opentrons.robot._driver.turn_on_blue_button_light()
        sleep(0.2)
    return('successfully blinked')

@app.route('/transfer/<int:volume>/<string:from_labware>/<int:from_col_number>/<string:to_labware>/<int:to_col_number>')
def transfer(volume,from_labware,from_col_number,to_labware,to_col_number): #transfer volume from a labware's column to another, picks up tips if needed
    if pipette.has_tip == False:
            pipette.pick_up_tip(presses = 1)
    labware1 = labware_dictionary[from_labware]
    labware2 = labware_dictionary[to_labware]
    pipette.transfer(
            volume,
            labware1.columns(str(from_col_number)),
            labware2.columns(str(to_col_number)),
            new_tip='never',
            mix_after=(3, 100))
    gotobin()
    pipette.blow_out()
    return('done gotcha')

@app.route('/dilute/<int:volume>/<string:sampling_labware_name>/<int:trough_col>/<int:sampling_col>/<int:cyto_col>')
@app.route('/dilute/<int:volume>/<string:sampling_labware_name>/<int:trough_col>/<int:sampling_col>/<int:cyto_col>/<int:keep_tips>')
def dilute(volume,sampling_labware_name,trough_col,sampling_col,cyto_col, keep_tips=False): #Add water to the cytometer's plate and dilute a volume of sample in it
    if pipette.has_tip:
        if not keep_tips:
            pipette.drop_tip()
            gotobin()
            pipette.pick_up_tip(presses = 1)
    else:
        pipette.pick_up_tip(presses = 1)
    gotobin()
    pipette.move_to(cytoplate.columns('1'))
    pipette.transfer(
            200-volume,
            trough.columns(str(trough_col)),
            cytoplate.columns(str(cyto_col)),
            new_tip='never')
    pipette.transfer(
            volume,
            labware_dictionary[sampling_labware_name].columns(str(sampling_col)),
            cytoplate.columns(str(cyto_col)),
            new_tip='never',
            mix_after=(3, 100))
    gotobin()
    pipette.blow_out()
    return('done gotcha')

@app.route('/dilute_cytoplate/<int:volume>/<int:trough_col>/<int:cyto_col>')
def dilute_cytoplate(volume, trough_col, cyto_col):
    gotobin()
    pipette.blow_out()
    if pipette.has_tip == False:
            pipette.pick_up_tip(presses = 1)
    pipette.transfer(
            200-volume,
            cytoplate.columns(str(cyto_col)),
            waste_funnel.columns('1'),
            new_tip='never')
    pipette.blow_out()
    pipette.transfer(
            200-volume,
            trough.columns(str(trough_col)),
            cytoplate.columns(str(cyto_col)),
            new_tip='never',
            mix_after=(3, 100))
    gotobin()
    pipette.blow_out()
    return('done gotcha')

@app.route('/wash_from_trough/<int:col_number>/<int:cyto_col>')
def wash(col_number,cyto_col): #Wash the cytometer's plate with water from the 12-well-through
    gotobin()
    pipette.blow_out()
    if pipette.has_tip == False:
            pipette.pick_up_tip(presses = 1)
    pipette.transfer(
            250,
            cytoplate.columns(str(cyto_col)),
            waste_funnel.columns('1'),
            new_tip='never')
    pipette.blow_out()
    pipette.transfer(
            200,
            trough.columns(str(col_number)),
            cytoplate.columns(str(cyto_col)),
            new_tip='never',
            mix_after=(3, 100))
    pipette.transfer(
            250,
            cytoplate.columns(str(cyto_col)),
            waste_funnel.columns('1'),
            new_tip='never')
    pipette.blow_out()
    return('done gotcha')

@app.route('/move_to/<string:labware>/<int:col_number>')
def moveto(labware,col_number): #moves to a labware's column
    pipette.move_to(labware_dictionary[labware].columns(str(col_number)))
    return('done gotcha')

@app.route('/gotorow/<int:row_number>')
def gotorow(row_number): #move to a column to sample from the bioreactors
    pipette.move_to(sampling_metal.columns(str(row_number)))
    return('success /gotorow/'+str(row_number))

@app.route('/gotowell/<string:well>')
def gotowell(well): #move to a well to sample from the bioreactors (only used with 4 bioreactors active to fill the whole column
    pipette.move_to(sampling_metal(well))
    return('success /gotowell/'+well)

@app.route('/gotobin')
def gotobin(): #moves to the liquid bin slot (the one in slot '3')
    pipette.move_to(waste_funnel.columns('1'))
    return('success trash')

@app.route('/home')
def gohome(): #homes the robot
    opentrons.robot.home()
    return('success?')

@app.route('/set_temperature/<int:T>')
def cold_as_ice(T): #detects the temperature module and set its temperature
    module = opentrons.modules.load('tempdeck', '10')
    cold_plate = opentrons.labware.load('96-flat', '10', share=True)
    module.set_temperature(T)
    return('freezin rn')

