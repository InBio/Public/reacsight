
For control of the opentrons OT-2 robot, the Flask interface and the instrument control code are in a single file `flaskr.py`.

It uses the Opentrons API version 1.

We typically run the Flask app from a terminal of the Jupyter notebook server that natively runs on the OT-2.

If the OT-2 is connected via USB, only the computer connected to it will be able to send HTTP requests to the OT-2 Flask app, so it logical to make this computer the experiment control computer.
