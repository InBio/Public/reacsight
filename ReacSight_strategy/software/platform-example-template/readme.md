
This folder provides an example that can serve as a template for a *ReacSight* platform.

It was used to perform automated cytometry and reactive optogenetic control for a Chi.Bio bioreactor array.

The main acquisition loop logic is defined in `loop.py`, the class to store reactor data and manage reactive experiment control is defined in `reactor.py`, and the functions enabling multi-instrument control with exhaustive single-file logging is defined in `instruments.py`.

An example experiment to run on this platform is defined and executed using *run.ipynb*. Because the acquisition lives in a python thread, executing the experiment in a jupyter notebook is handy to interact with the parameters and code of the experiment during execution (and pause it if need be).

### Hardware

* A Chi.Bio reactor array (one controller, 8 reactors, 6 pump boards) (can be purchased [here](https://www.labmaker.org/products/chi-bio))
* Opentrons OT-2 robot
* Benchtop cytometer (we used Guava EasyCyte HT 14 BGV)

### Dependencies and compatible environments

* Python 3.6+
* pandas 1.0.1+
* requests 2.25.1+

Successfully tested on Windows 7 and Windows 10 with miniconda-managed Python 3 distributions.
