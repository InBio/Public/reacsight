from time import time
import pandas as pd
import os

class BaseProgram:
    def __init__(self, reactor_id, dilution_strength=0., blue_light_intensity=0.):
        # creating folder for data output
        self.reactor_id = reactor_id
        self.output_dir = f'data/reactor-{self.reactor_id}'
        os.makedirs(self.output_dir)
        # two reactor parameters (change be changed by events at every loop)
        self.dilution_strength = dilution_strength
        self.blue_light_intensity = blue_light_intensity
        # the events for this reactor, by type of events
        self.OD_events = []
        self.cytometry_events = []
        self.generic_events = []
        # data storage
        self.loop_data = []
        self.OD_data = [] # list of dict {t:, OD:, n_loop:}
        self.dilution_data = [] # list dict {t:, dilution_strength:, n_loop:}
        self.blue_light_data = [] # list dict {t:, intensity:, n_loop:}
        self.cytometry_data = [] # list dict {t:, data:,  n_loop:, t_sampling:}
    def receive_OD_data(self, OD, n_loop, t_start):
        # store data
        self.OD_data.append({'t_rel_hrs':(time()-t_start)/3600, 'OD':OD, 'n_loop':n_loop})
        # check OD events
        for event in self.OD_events:
            event.check_and_apply()
    def send_dilution_needs(self, n_loop, t_start):
        # store, send to main loop
        self.dilution_data.append({'t_rel_hrs':(time()-t_start)/3600, 'dilution_strength':self.dilution_strength, 'n_loop':n_loop})
        return self.dilution_strength
    def receive_effective_dilution_data(self, effective_dilution, n_loop, t_start):
        self.effective_dilution_data.append({'t_rel_hrs':(time()-t_start)/3600, 'OD':effective_dilution, 'n_loop':n_loop})
    def send_blue_light_needs(self, n_loop, t_start):
        # store, send to main loop
        self.blue_light_data.append({'t_rel_hrs':(time()-t_start)/3600, 'intensity':self.blue_light_intensity, 'n_loop':n_loop})
        return self.blue_light_intensity
    def receive_cytometry_data(self, cyto_data, n_loop, t_start, t_sampling):
        # store data
        t_rel_hrs = (time()-t_start)/3600
        t_sampling_rel_hrs = (t_sampling-t_start)/3600
        cyto_data['t_rel_hrs'] = t_rel_hrs
        cyto_data['t_sampling_rel_hrs'] = t_sampling_rel_hrs
        self.cytometry_data.append({'t_rel_hrs':t_rel_hrs, 't_sampling_rel_hrs':t_sampling_rel_hrs, 'data':cyto_data, 'n_loop':n_loop})
        # write cyto data
        cyto_data.to_csv(f'{self.output_dir}/cytometry_{len(self.cytometry_data)}.csv', index=False)
        # check cytometry events
        for event in self.cytometry_events:
            event.check_and_apply()
    def write_data(self):
        # write OD and dilution data
        pd.DataFrame.from_dict(self.OD_data).to_csv(f'{self.output_dir}/OD.csv', index=False)
        pd.DataFrame.from_dict(self.dilution_data).to_csv(f'{self.output_dir}/dilution.csv', index=False)
        pd.DataFrame.from_dict(self.blue_light_data).to_csv(f'{self.output_dir}/blue_light.csv', index=False)

class Event:
    def __init__(self, name, trigger, action, program):
        self.name = name
        self.trigger = trigger
        self.action = action
        self.program = program
    def check_and_apply(self):
        if self.trigger(self.program):
            self.action(self.program)
