# this module could be use as a single access point to Flask interfaces of all instruments
# it also write an experiment-level log file
import requests
import logging

# IP:port addresses of the different instruments
instrument_addresses = {'chibios':'[IP:PORT]',
                        'OT2':'[IP:PORT]',
                        'cytometer':'[IP:PORTS]'}

# same for mock instruments, used only in mock mode
mock_mode = False
mock_instrument_addresses = {'chibios':'http://localhost:[PORT]/chibios',
                             'OT2':'http://localhost:[PORT]/OT2',
                             'cytometer':'http://localhost:[PORT]/cytometer'}

# to post experiment info on discord channel
experiment_info_discord_webhook = 'url'

# unique id for commands
next_command_id = 1

# to run at the beginning of an experiment, ask instruments for their version and log it
def ask_and_log_instrument_versions():
    addresses = mock_instrument_addresses if mock_mode else instrument_addresses
    for instrument_name,instrument_address in addresses.items():
        instrument_version = requests.get(f'{instrument_address}/version').text
        logging.info(f'Instrument {instrument_name} version: {instrument_version} (mock: {mock_mode})')

# generic instrument command method, with logging and request handling
def send_command(instrument_name, command, expect_file=False):
    global next_command_id
    command_id = next_command_id
    next_command_id += 1
    instrument_address = mock_instrument_addresses[instrument_name] if mock_mode else instrument_addresses[instrument_name]
    logging.info(f'#{command_id} TO {instrument_name}: {command}')
    try:
        instrument_response = requests.get(f'{instrument_address}/{command}')
        instrument_response.raise_for_status()
    except requests.exceptions.RequestException as e:
        logging.error(f'Request exception for command #{command_id}: {e}')
        raise
    except BaseException as e:
        logging.error(f'Unknown exception for command #{command_id}: {e}')
        raise
    if not expect_file:
        logging.info(f'{instrument_name} RESPONSE - {command} (#{command_id}): {instrument_response.text}')
        return instrument_response.text
    else:
        logging.info(f'{instrument_name} FILE RECEIVED - {command} (#{command_id})')
        return instrument_response.content

def send_info(info_text):
    try:
        requests.post(experiment_info_discord_webhook, data={'content':info_text})
    except BaseException as e:
        logging.error(f'Discord info posting error: {e}')
