from time import time, sleep
from instruments import send_command, send_info
import logging
import requests
import threading
import os
import guava2data # specific for parsing our fcs files

## loop parameters
loop_duration_s = 120.

## loop variables
reactor_programs = {} # programs indexed by reactor slots unique ids
measurement_schedule = [] # measurement (cytometry) times as seconds form start exp time
ongoing_measurement = False
sampling_plate = 1
sampling_col = 12
sampling_start_time = 0
num_loops_done = 0
start_exp_time = 0
thread = None
continue_looping = False

def loop():
    global num_loops_done, thread, continue_looping, reactor_programs, start_exp_time, ongoing_measurement, sampling_plate, sampling_col, sampling_start_time
    if num_loops_done == 0:
        start_exp_time = time()
    start_loop_time = time()
    logging.info(f'LOOP #{num_loops_done}')
    ### reactors management
    try:
        ## OD reading
        od_command_str = '|'.join([str(rid) for rid in reactor_programs])
        all_ods = send_command('chibios', f'OD/{od_command_str}')
        ## extracting reactor-specific OD and sending it to corresponding reactor program
        for rid_od_str in all_ods.split('|'):
            rid_str,od_str = rid_od_str.split('_')
            reactor_programs[int(rid_str)].receive_OD_data(float(od_str), num_loops_done, start_exp_time)
        ## dilutions
        dilutions = '|'.join([f'{rid}_{program.send_dilution_needs(num_loops_done, start_exp_time)}' for rid,program in reactor_programs.items()])
        send_command('chibios', f'pumps/dilute/{dilutions}')
        ## blue light
        leds = '|'.join([f'{rid}_B_{program.send_blue_light_needs(num_loops_done, start_exp_time)}' for rid,program in reactor_programs.items()])
        send_command('chibios', f'LEDs/{leds}')
        ### measurement (cytometry) management
        if not ongoing_measurement:
            ## check if should do new measurement
            if measurement_schedule and time() - start_exp_time > min(measurement_schedule):
                send_info('starting new cytometry measurement...')
                ongoing_measurement = True
                measurement_schedule.remove(min(measurement_schedule))
                ## dead volume
                send_command('OT2', 'gotobin')
                rids = '|'.join([str(rid) for rid in reactor_programs])
                send_command('chibios', f'pumps/sampling/dead_volume/{rids}')
                # sampling: go over sampling plate, do the sampling
                send_command('OT2', f'move_to/sampling_metal_chi_{sampling_plate}/{sampling_col}')
                send_command('chibios',f'pumps/sampling/sample/{rids}')
                sampling_start_time = time()
                # now we can does the whole cytometry (preparation, loading, acquisition, wash)
                cyto_thread = threading.Thread(target=do_cytometry)
                cyto_thread.start()
        ### generic events management
        for program in reactor_programs.values():
            for event in program.generic_events:
                event.check_and_apply()
    except BaseException as e:
        send_info(f'Error during reactor management, stopping there ({e})')
        continue_looping = False
        return
    ### data writing
    try:
        for rid,program in reactor_programs.items():
            program.write_data()
    except BaseException as e:
        send_info(f'Error during data writing ({e}), continuing anyway')
    ### end of loop (wait if needed)
    while continue_looping and time() - start_loop_time < loop_duration_s:
        sleep(1)
    num_loops_done += 1
    if continue_looping:
        thread = threading.Thread(target=loop)
        thread.start()

def start():
    global continue_looping, thread
    continue_looping = True
    print('starting looping !')
    thread = threading.Thread(target = loop)
    thread.start()

def stop():
    global continue_looping, thread
    continue_looping = False
    print('stopping, wait end of current loop...')
    thread.join()
    print('loop finished')

def do_cytometry():
    global num_loops_done, thread, continue_looping, reactor_programs, start_exp_time, ongoing_measurement, sampling_plate, sampling_col, sampling_start_time
    try:
        rid2well = {rid:row+'12' for rid,row in {1:'H',2:'G',3:'F',4:'E',5:'D',6:'C',7:'B',8:'A'}.items()}
        # preparing guava
        wells = '_'.join([rid2well[rid] for rid in reactor_programs])
        send_command('cytometer', f'prepare/{wells}/5000')
        # diluting into guava
        send_command('OT2', f'dilute/10/sampling_chi_{sampling_plate}/{sampling_col}/{sampling_col}/1')
        # acquisition
        send_command('cytometer', 'acquire')
        # ask regularly if acquisition is finished, get the raw file, extract reactor data
        sleep(60)
        while send_command('cytometer', 'status') != 'finished':
            sleep(60)
        raw_fcs = send_command('cytometer', 'retrieve_data', expect_file=True)
        # treatment of fcs data
        with open('raw.fcs', 'wb') as fcs_writing:
            fcs_writing.write(raw_fcs)
        fcs = guava2data.GuavaFCS3('raw.fcs')
        for rid,program in reactor_programs.items():
            df = fcs.get_sample(rid2well[rid]).to_df(min_n_return_none=10)
            if df is None:
                send_info(f'Not enough events for cytometry sample of reactor {rid} !')
            else:
                program.receive_cytometry_data(df, num_loops_done, start_exp_time, sampling_start_time)
        os.remove('raw.fcs')
        # do the washing
        send_command('cytometer', 'toggle_tray')
        for i in range(3):
            send_command('OT2', f'wash_plate_from_trough/{sampling_col}/cytoplate/1')
        send_command('cytometer', 'toggle_tray')
        # update info for next sampling col and status
        sampling_col -= 1
        if sampling_col < 1:
            sampling_col = 12
            sampling_plate += 1
        ongoing_measurement = False
        send_info('Got cytometry data and washed the plate !')
    except BaseException as e:
        send_info(f'Error during cytometry acquisition, stopping there ({e})')
        continue_looping = False
        return

def status():
    return f'looping:{continue_looping} -- loop thread status:{thread.is_alive()} -- total loops done:{num_loops_done}'
