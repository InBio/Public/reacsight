
import pandas as pd
import numpy as np

FP_signatures = {}
FP_signatures['mScarletI'] = pd.DataFrame.from_dict({'BLU-V': 0.0009763207053765655, 'GRN-V': 0.0004237286339048296, 'YEL-V': 0.0, 'ORG-V': 0.0026230504736304283, 'GRN-B': 0.01031692884862423, 'YEL-B': 0.02983197756111622, 'RED-B': 0.02653220295906067, 'NIR-B': 0.006912886630743742, 'YEL-G': 0.11395768821239471, 'ORG-G': 1.0, 'RED-G': 0.10561968386173248, 'NIR-G': 0.029573990032076836}, orient='index', columns=['mScarletI'])
FP_signatures['mNeonGreen'] = pd.DataFrame.from_dict({'BLU-V': 0.0, 'GRN-V': 0.0050149220041930676, 'YEL-V': 0.0006017238483764231, 'ORG-V': 0.0008070773328654468, 'GRN-B': 1.0, 'YEL-B': 0.13482020795345306, 'RED-B': 0.004472557455301285, 'NIR-B': 0.0007251722272485495, 'YEL-G': 0.0041534616611897945, 'ORG-G': 0.003998787607997656, 'RED-G': 9.872133523458615e-05, 'NIR-G': 0.0}, orient='index', columns=['mNeonGreen'])
FP_signatures['mCerulean'] = pd.DataFrame.from_dict({'BLU-V': 0.2074112743139267, 'GRN-V': 1.0, 'YEL-V': 0.13280613720417023, 'ORG-V': 0.31676194071769714, 'GRN-B': 0.05822112783789635, 'YEL-B': 0.0, 'RED-B': 0.0, 'NIR-B': 0.0, 'YEL-G': 0.0, 'ORG-G': 0.0, 'RED-G': 0.0, 'NIR-G': 0.0}, orient='index', columns=['mCerulean'])

def infer_FP_amounts(data, AF, signature_names=FP_signatures.keys(), signatures=None):
    # build fluo channels names
    ch_colors = list(FP_signatures['mScarletI'].index)
    ch_color_names = [ch + '-HLin' for ch in ch_colors]
    # build the matrix with with fluorescence vector as lines and cells as columns
    signal = (data[ch_color_names] - AF[ch_color_names]).transpose()
    # build the FP signature matrix
    if signatures is None:
        signatures = FP_signatures
    all_signatures = []
    for signature_name in signature_names:
        if signature_name in signatures:
            all_signatures.append(signatures[signature_name])
        else:
            all_signatures.append(FP_signatures[signature_name])
    A = pd.concat(all_signatures, axis=1)
    # solve the linear system
    result = np.linalg.lstsq(A,signal,rcond=None)
    # store result in the data frame and return
    N = result[0]
    R = result[1]
    for i,fp in enumerate(signature_names):
        data[fp] = N[i]
    data['deconv_residual'] = R
    return data
