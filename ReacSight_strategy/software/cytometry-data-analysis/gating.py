import numpy as np
from scipy.stats import gaussian_kde

def compute_ks_FSC_SSC_gating_metric(data, grid_size=100, return_grid=False):
    FSC_log = np.log10(data['FSC-HLin'])
    SSC_log = np.log10(data['SSC-HLin'])
    if not np.isfinite(FSC_log).all():
        raise Exception('FSC')
    if not np.isfinite(SSC_log).all():
        raise Exception('SSC')
    kernel = gaussian_kde([FSC_log, SSC_log])
    extent= [np.min(FSC_log),
             np.max(FSC_log),
             np.min(SSC_log),
             np.max(SSC_log)]
    FSC_grid, SSC_grid = np.mgrid[extent[0]:extent[1]:grid_size*1j,
                                  extent[2]:extent[3]:grid_size*1j]
    positions = np.vstack([FSC_grid.ravel(), SSC_grid.ravel()])
    P = np.reshape(kernel(positions).T, FSC_grid.shape)
    FSC_bin = np.floor((FSC_log-extent[0])*grid_size/(extent[1]-extent[0]))
    SSC_bin = np.floor((SSC_log-extent[2])*grid_size/(extent[3]-extent[2]))
    FSC_bin[FSC_bin == grid_size] = grid_size - 1
    SSC_bin[SSC_bin == grid_size] = grid_size - 1
    P_max = np.max(P)
    gating_metric = np.array([P[fsc_bin, ssc_bin] for fsc_bin,ssc_bin in zip(FSC_bin.astype(int),SSC_bin.astype(int))]) / P_max
    data['gating-metric'] = gating_metric
    if return_grid:
        return data, positions, P
    return data

def compute_doublet_metric(data):
    fsch_to_fsca_fit = np.polyfit(data['FSC-HLin'], data['FSC-ALin'], 1)
    pred_fsca = np.polyval(p=fsch_to_fsca_fit, x=data['FSC-HLin'])
    deviation = pred_fsca - data['FSC-ALin']
    std_deviation = np.std(deviation)
    data['doublet-metric'] = np.abs(deviation) / std_deviation
    return data
