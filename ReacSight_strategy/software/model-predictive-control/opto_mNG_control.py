
import numpy as np
from  scipy.optimize import minimize

# model simulation using the analytical expression (valid for fixed light intensity)
def model_analytic(model_pars, light_intensity, y0, duration):
    mrna_0,fp_0 = y0
    delta_light = light_intensity / 40.
    expo_deg_m = np.exp(- model_pars['deg_m'] * duration)
    mrna = mrna_0 * expo_deg_m  + delta_light * (1. - expo_deg_m)
    expo_deg_fp = np.exp(- model_pars['deg_fp'] * duration)
    fp = delta_light * model_pars['sigma'] / model_pars['deg_fp']
    fp += model_pars['sigma'] * (delta_light - mrna_0) / (model_pars['deg_m'] - model_pars['deg_fp']) * expo_deg_m
    fp += (fp_0 - delta_light * model_pars['sigma'] / model_pars['deg_fp']) * expo_deg_fp
    fp -= model_pars['sigma'] * (delta_light - mrna_0) / (model_pars['deg_m'] - model_pars['deg_fp']) * expo_deg_fp
    return np.array([mrna,fp])

# simulate a given light profile (list of tuples (intensity, durations))
def simulate_analytic(model_pars, light_profile, y0, t0=0, n_evals=2):
    y = y0
    ys = [y]
    ts = [t0]
    fp_at_edges = [y0[-1]]
    t_at_edges = [t0]
    for intensity,duration in light_profile:
        if duration > 0:
            delta_t = duration / (n_evals - 1.)
            for n in range(n_evals - 1):
                y = model_analytic(model_pars, intensity, y, delta_t)
                ys.append(y)
                ts.append(ts[-1]+delta_t)
        fp_at_edges.append(y[-1])
        t_at_edges.append(ts[-1])
    y = np.vstack(ys).T
    t = np.array(ts)
    return t, y, np.array(fp_at_edges), np.array(t_at_edges)

# class to perform the MPC optimization of duty fractions
class DutyCycleController:
    def __init__(self, target, model_pars, initial_state, dc_period_hrs=0.5, n_cycles_horizon=10, intensity=40):
        self.target = target
        self.model_pars = model_pars
        self.initial_state = initial_state
        self.current_state = initial_state
        self.dc_period_hrs = dc_period_hrs
        self.n_cycles_horizon = n_cycles_horizon
        self.intensity = intensity
    ## utils
    def dcs_to_light_profile(self, dcs):
        light_profile = []
        for dc in dcs:
            light_profile.append((self.intensity, dc*self.dc_period_hrs))
            light_profile.append((0., (1-dc)*self.dc_period_hrs))
        return light_profile
    ## controller optimization
    def cost_dc_optim(self, dcs):
        light_profile = self.dcs_to_light_profile(dcs)
        _,_,fp_at_edges,_ = simulate_analytic(model=self.model, model_pars=self.model_pars, light_profile=light_profile, y0=self.current_state)
        return np.square(fp_at_edges[1:-1:2]-self.target).sum()
    def optimize(self):
        res = minimize(fun=self.cost_dc_optim,
                        x0=[0.5]*self.n_cycles_horizon,
                        args=(),
                        method='L-BFGS-B',
                        bounds=[(0.,1.)]*self.n_cycles_horizon)
        return res.x
    def apply_dcs(self, dcs, t0=0., n_evals=2):
        light_profile = self.dcs_to_light_profile(dcs)
        t,y,_ = self.sim_fun(model=self.model, model_pars=self.model_pars, light_profile=light_profile, y0=self.current_state, t0=t0, n_evals=n_evals)
        self.current_state = y[:,-1]
        return t,y
