## loading cytometry and LEDs data
import pandas as pd
print('loading data...')
raw_dfs = {'target_20pc':pd.read_csv('raw_data/inbioreactors-2020-11-05/reactor-5/cells.csv'),
           'target_40pc':pd.read_csv('raw_data/inbioreactors-2020-11-05/reactor-6/cells.csv'),
           'target_60pc':pd.read_csv('raw_data/inbioreactors-2020-11-05/reactor-7/cells.csv'),
           'target_80pc':pd.read_csv('raw_data/inbioreactors-2020-11-05/reactor-8/cells.csv'),
           'AF_ref':pd.read_csv('raw_data/inbioreactors-2020-06-23/reactor-5/cells.csv')}
raw_LED_dfs = {'target_20pc':pd.read_csv('raw_data/inbioreactors-2020-11-05/reactor-5/LEDs.csv'),
             'target_40pc':pd.read_csv('raw_data/inbioreactors-2020-11-05/reactor-6/LEDs.csv'),
             'target_60pc':pd.read_csv('raw_data/inbioreactors-2020-11-05/reactor-7/LEDs.csv'),
             'target_80pc':pd.read_csv('raw_data/inbioreactors-2020-11-05/reactor-8/LEDs.csv')}

## size gating
print('size gating (a bit slow, patience...)')
from lib import analysis
gated_dfs = {}
for name,df in raw_dfs.items():
    print(name)
    gated_dfs[name] = analysis.filter_data(data=df, gating_threshold=0.8, doublet_threshold=0.5)
    
# deconvolution
from lib import deconvolution
AF_medians = gated_dfs['AF_ref'].mean()
for name, df in gated_dfs.items():
    df = deconvolution.infer_FP_amounts(AF=AF_medians, data=df, signature_names=['mCerulean', 'mNeonGreen', 'mScarletI'])
    df['mNeonGreen_conc'] = df['mNeonGreen'] / df['FSC-HLin'] * 0.39235 # RPU conversion factor
    gated_dfs[name] = df
    
## plotting preparation
import matplotlib.pyplot as plt
import numpy as np
plt.rcParams['font.size'] = 15
plt.rcParams['axes.grid'] = True 
plt.rcParams['markers.fillstyle'] = 'none' 
plt.rcParams['lines.markersize'] = 3
plt.rcParams['lines.markeredgewidth'] = 1
plt.rcParams['lines.linewidth'] = 1.25
plt.rcParams['axes.linewidth'] = 1.25
plt.rcParams['figure.figsize'] = (15,10)

## plotting the control results
colors = plt.cm.jet(np.linspace(0.,0.3,4))
f, axs = plt.subplots(figsize=(9*0.75,8*0.75), ncols=1, nrows=2)
for i,target in enumerate(['target_20pc', 'target_40pc', 'target_60pc', 'target_80pc']):
    df = gated_dfs[target]
    df_LEDs = raw_LED_dfs[target]
    t0 = df_LEDs.query('intensity > 0')['time_s'].iloc[0]
    ts = [t0]
    intensities = [0]
    tend = t0 + 9.02*3600
    for _,row in df_LEDs.query(f'time_s <= {tend}').iterrows():
        if row['intensity'] != intensities[-1]:
            ts += [row['time_s'], row['time_s']]
            intensities += [intensities[-1], row['intensity']]
    df_medians = df.query(f'time_s < {tend}').groupby('time_s').median()
    axs[0].plot((df_medians.index-t0)/3600, df_medians['mNeonGreen_conc'], '-o', color=colors[i], markersize=6, markerfacecolor = colors[i])
    target_float = float(target.split('_')[1].split('pc')[0])/100.
    axs[0].plot([0,23], [target_float]*2, '--', color=colors[i])
    axs[1].plot((np.array(ts)-t0)/3600,np.array(intensities)/40 + i*2, color=colors[i])
axs[1].yaxis.set_ticks([])
axs[0].xaxis.set_ticks([])
axs[1].set(xlim=(-1,10), ylim=(-1,8), ylabel='light intensity', xlabel='Time (hrs)')
axs[0].set(xlim=(-1,10), ylabel='mNeonGreen / FSC (RPU)', ylim=(-0.05,1))
for ax in axs:
    ax.grid()
plt.savefig('plots/EL222_MPC_results.pdf')

## plotting single cells
# single plot fun
def plot_all_cells(cells, ch_fl='mNeonGreen_conc',ax=None,ylim=(0.01,1),delta_i_per_hr=1000,log_scale=True,filter_str=None,down_sampling=None):
    if ax is None:
        f,ax = plt.subplots(figsize=(14,8), ncols=1, nrows=1)
    if filter_str is not None:
        cells = cells.query(filter_str).reset_index()
    n_tp = len(cells.groupby('time_s'))
    colors = plt.cm.jet(np.linspace(0,1,n_tp))
    i_tp = 0
    delta_i = 0
    cum_i = 0
    tp_prev = cells['time_s'].iloc[0]
    for tp,d in cells.groupby('time_s'):
        delta_i += (tp-tp_prev) / 3600 * delta_i_per_hr
        if down_sampling is not None:
            d = d.iloc[-down_sampling:]
        ax.scatter(np.arange(len(d)) + cum_i + delta_i, d[ch_fl], s=0.1, c=np.array([colors[i_tp]]))
        tp_prev = tp
        cum_i += len(d)
        i_tp += 1
    ax.set_ylim(ylim)
    if log_scale:
        ax.set_yscale('log')
    ax.grid()
    ax.xaxis.set_ticks([])
    return ax
# make full plot
f,axs = plt.subplots(ncols=2, nrows=2, figsize=(8,6))
axs = axs.ravel()
for i,target in enumerate(['target_20pc', 'target_40pc', 'target_60pc', 'target_80pc']):
    plot_all_cells(gated_dfs[target].query(f'time_s < {tend}'), ax=axs[i], log_scale=False, ylim=(-0.1,1.3))
    target_float = float(target.split('_')[1].split('pc')[0])/100.
    axs[i].plot([0,2.e4],[target_float]*2, 'k')
axs[0].set(ylabel='mNeonGreen / FSC (RPU)')
axs[2].set(ylabel='mNeonGreen / FSC (RPU)')
f.tight_layout()
plt.savefig('plots/EL222_MPC_single_cells.pdf')