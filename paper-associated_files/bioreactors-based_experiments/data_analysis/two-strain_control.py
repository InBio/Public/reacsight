## loading cytometry and OD data
import pandas as pd
import numpy as np
ratio_targets = {1: 0.125, 2: 0.25, 3: 0.5, 4: 1, 5: 1, 6: 2, 7: 4, 8: 8}
raw_dfs = {}
ods_dfs = {}
leds_dfs = {}
for rid in range(1,9):
    raw_dfs[rid] = pd.read_csv(f'raw_data/inbioreactors-2021-03-04/reactor-{rid}/cells.csv')
    ods_dfs[rid] = pd.read_csv(f'raw_data/inbioreactors-2021-03-04/reactor-{rid}/ODs.csv')
    leds_dfs[rid] = pd.read_csv(f'raw_data/inbioreactors-2021-03-04/reactor-{rid}/LEDs.csv')
    
## function computing the ratio of two strain from cytometry data
def compute_ratio(df):
    n_green = sum(np.logical_and(df['GRN-B-HLin'] > 300, df['ORG-G-HLin']< 150))
    n_others = len(df) - n_green
    ratio = n_green/n_others
    return ratio

## making the figure
import matplotlib.pyplot as plt
rid_sel = [2,3,4,6,7]
f, (ax1,ax2) = plt.subplots(ncols=1, nrows = 2, figsize=(8,6))
colors = plt.cm.jet(np.linspace(0,1,len(rid_sel)))
for i,rid in enumerate(rid_sel):
    tref = leds_dfs[rid].query('intensity == 40').iloc[0]['time_s']
    # ratios
    tps,ratios = [],[]
    for t,df in raw_dfs[rid].groupby('time_s'):
        trel = (t-tref)/3600
        if trel < 42 and (trel < 19.5 or trel > 20.7): # exclude the error scheduled timepoint (although data is consistent)
            ratio = compute_ratio(df)
            tps.append(trel)
            ratios.append(ratio)
    ax1.semilogy(tps, ratios, '-o', color=colors[i])
    ax1.semilogy(tps, [ratio_targets[rid]]*len(tps), '--', label=None, color=colors[i])
    # od
    od = ods_dfs[rid].query(f'time_s < {tref+3600*41.4}')
    ax2.plot((od['time_s']-tref)/3600, (od['OD']-0.3) + i*1.2, color=colors[i])
    ax2.axhspan(0+i*1.2,0.7+i*1.2,color=colors[i], alpha=0.2)
    
ax1.set(ylim=(0.1,6), xlim=(-0.5,41.5), ylabel='WT his- / slow his+')
ax2.set(ylim=(-0.5,6), xlim=(-0.5,41.5), ylabel='OD control', xlabel='Time (hrs)')
ax2.yaxis.set_ticks([])
ax1.xaxis.set_ticks([])
ax1.yaxis.set_ticks([0.25, 0.5, 1, 2, 4])
ax1.yaxis.set_ticklabels([0.25, 0.5, 1, 2, 4])
f.tight_layout()
f.savefig('plots/two_strain_control_by_OD.pdf')