## loading cytometry and LEDs data
import pandas as pd
print('loading data...')
raw_dfs = {'AF_previous':pd.read_csv('raw_data/inbioreactors-2020-06-23/reactor-5/cells.csv'),
           'mCer_previous':pd.read_csv('raw_data/inbioreactors-2020-06-23/reactor-4/cells.csv'),
           'mNG_previous':pd.read_csv('raw_data/inbioreactors-2020-06-23/reactor-8/cells.csv'),
           'mScI_previous':pd.read_csv('raw_data/inbioreactors-2020-06-23/reactor-6/cells.csv'),
           'opto-1':pd.read_csv('raw_data/inbioreactors-2020-10-26/reactor-1/cells.csv'),
           'opto-2':pd.read_csv('raw_data/inbioreactors-2020-10-26/reactor-2/cells.csv'),
           'opto-3':pd.read_csv('raw_data/inbioreactors-2020-10-26/reactor-3/cells.csv')}
raw_LED_dfs = {'opto-1':pd.read_csv('raw_data/inbioreactors-2020-10-26/reactor-1/LEDs.csv'),
               'opto-2':pd.read_csv('raw_data/inbioreactors-2020-10-26/reactor-2/LEDs.csv'),
               'opto-3':pd.read_csv('raw_data/inbioreactors-2020-10-26/reactor-3/LEDs.csv')}

## size gating
print('size gating (a bit slow, patience...)')
from lib import analysis
gated_dfs = {}
for name,df in raw_dfs.items():
    print(name)
    gated_dfs[name] = analysis.filter_data(data=df, gating_threshold=0.8, doublet_threshold=0.5)
        
## deconvolution
from lib import deconvolution
AF_medians = gated_dfs['AF_previous'].mean()
for name, df in gated_dfs.items():
    df = deconvolution.infer_FP_amounts(AF=AF_medians, data=df, signature_names=['mCerulean', 'mNeonGreen', 'mScarletI'])
    # channel renaming
    for ch_long,ch_short in {"GRN-B-HLin":"GRNB", "GRN-V-HLin":"GRNV", "ORG-G-HLin":"ORGG", "FSC-HLin":"FSC"}.items():
        df[ch_short] = df[ch_long]
    for fp in ['mCerulean', 'mNeonGreen', 'mScarletI', 'GRNB', 'GRNV', 'ORGG']:
        df[f'{fp}_conc_raw'] = df[fp] / df['FSC']
    gated_dfs[name] = df

## conversion in RPU units
# calculate the conversion factors
fp_conc_conversion = {}
fp_short = {'mCerulean':'mCer', 'mNeonGreen':'mNG', 'mScarletI':'mScI'}
for fp,ch in zip(['mCerulean', 'mNeonGreen', 'mScarletI'], ['GRNV','GRNB','ORGG']):
    df = gated_dfs[f'{fp_short[fp]}_previous']
    fp_conc_conversion[fp] = 1. / df[f'{fp}_conc_raw'].median()
    fp_conc_conversion[ch] = 1. / df[f'{ch}_conc_raw'].median()
# apply
for name in ['opto-1', 'opto-2', 'opto-3']:
    df = gated_dfs[name]
    for fp in ['mCerulean', 'mNeonGreen', 'mScarletI', 'GRNV', 'GRNB', 'ORGG']:
        df[f'{fp}_conc'] = df[f'{fp}_conc_raw'] * fp_conc_conversion[fp]
        
## plotting preparation
import matplotlib.pyplot as plt
import numpy as np
plt.rcParams['font.size'] = 15
plt.rcParams['axes.grid'] = True 
plt.rcParams['markers.fillstyle'] = 'none' 
plt.rcParams['lines.markersize'] = 3
plt.rcParams['lines.markeredgewidth'] = 1
plt.rcParams['lines.linewidth'] = 1.25
plt.rcParams['axes.linewidth'] = 1.25
plt.rcParams['figure.figsize'] = (15,10)
strain2color = {'mScI':'red', 'AF':'black', 'mCer':'blue', 'mNG':'green', '3-colors':'purple'}

## plotting EL222 dynamics (median of cells)
colors = plt.cm.jet(np.linspace(0.,0.3,3))
f, axs = plt.subplots(figsize=(12*0.75,8*0.75), ncols=1, nrows=2)
for i,cond in enumerate(['opto-1', 'opto-2', 'opto-3']):
    df = gated_dfs[cond]
    df_LEDs = raw_LED_dfs[cond]
    t0 = df_LEDs.query('intensity > 0')['time_s'].iloc[0]
    ts = [t0-3600,t0]
    intensities = [0,0]
    tend = df['time_s'].max()
    for _,row in df_LEDs.query(f'time_s <= {tend}').iterrows():
        if row['intensity'] != intensities[-1]:
            ts += [row['time_s'], row['time_s']]
            intensities += [intensities[-1], row['intensity']]
    ts.append(tend)
    intensities.append(intensities[-1])
    df_medians = df.groupby('time_s').median()
    axs[0].plot((df_medians.index-t0)/3600, df_medians['mNeonGreen_conc'], '--o', color=colors[i], markersize=6, markerfacecolor = colors[i])
    axs[1].plot((np.array(ts)-t0)/3600,np.array(intensities)/40 + i*2, color=colors[i])
axs[1].yaxis.set_ticks([])
axs[0].xaxis.set_ticks([])
axs[1].set(xlim=(-1,16), ylim=(-1,6), ylabel='light intensity', xlabel='Time (hrs)')
axs[0].set(xlim=(-1,16), ylabel='mNeonGreen/FSC (RPU)', ylim=(-0.05,1.3))
for ax in axs:
    ax.grid()
f.tight_layout()
plt.savefig('plots/EL222_median_dynamics.pdf')

## change of distributions EL222
from scipy.stats import gaussian_kde
f,ax = plt.subplots(ncols=1,nrows=1, figsize=(7,4))

df_LEDs = raw_LED_dfs['opto-1']
t0 = df_LEDs.query('intensity > 0')['time_s'].iloc[0]
df = gated_dfs['opto-1'].query(f'time_s < {t0 + 4.1*3600}').query(f'time_s > {t0}')
ch = 'mNeonGreen'
colors = plt.cm.winter(np.linspace(0,1,7))
i_tp = 0
for tp,df_tp in df.groupby('time_s'):
    if i_tp < 7:
        kernel = gaussian_kde(df_tp[f'{ch}_conc'])
        x = np.linspace(-0.1, 2, 5000)
        density = kernel.evaluate(x)
        tp_rel = (tp-t0)/3600
        ax.plot(x, density/density.max(), color=colors[i_tp], label=f'T = {tp_rel:.01f} hrs', linewidth=2)
        i_tp += 1
ax.set(xlabel=f'{ch} / FSC (RPU)', ylabel='relative density')
ax.grid()        
f.tight_layout()
ax.legend()
plt.savefig('plots/EL222_distribution_dynamics.pdf')


## prepare data for model fitting
from lib import simulation as sim
datapoints_list = []
light_profile_list = []
is_timepoint_list = []
t_measurements_abs_list = []
t_measurements_list = []
leds_list = []
for i in range(3):
    cond = f'opto-{i+1}'
    df = gated_dfs[cond]
    df_LEDs = raw_LED_dfs[cond]
    t0 = df_LEDs.query('intensity > 0')['time_s'].iloc[0]
    ts = [t0-3600,t0]
    intensities = [0,0]
    tend = df['time_s'].max()
    for _,row in df_LEDs.query(f'time_s <= {tend}').iterrows():
        if row['intensity'] != intensities[-1]:
            ts += [row['time_s'], row['time_s']]
            intensities += [intensities[-1], row['intensity']]
    ts.append(tend)
    intensities.append(intensities[-1])
    leds_list.append(((np.array(ts)-t0)/3600,np.array(intensities)/40))
    df_medians = df.groupby('sampling_time_s').median()
    light_profile = sim.led_history_to_light_profile(df_leds=df_LEDs, t0_abs_s=t0-3600, tend_abs_s=t0+17*3600, intensity_at_t0=0)
    t_measurements = list((df_medians.index-t0)/3600)
    t_measurements = [t for t in t_measurements if t < 16] # remove above t>16 because plate issue
    t_measurements_list.append(t_measurements)
    t_measurements_abs = [t-t_measurements[0] for t in t_measurements] # start at first
    light_profile,is_timepoint = sim.integrate_timepoints_in_light_profile(light_profile, t_measurements_abs)
    light_profile_list.append(light_profile)
    is_timepoint_list.append(is_timepoint)
    t_measurements_abs_list.append(t_measurements_abs)
    datapoints_list.append(df_medians['mNeonGreen_conc'].iloc[:len(t_measurements_abs)])
    
## model fitting
from lib import EL222_model as model
from  scipy.optimize import minimize
def cost_fun_fit(x):
    pars = {'sigma':x[0], 'deg_m':x[1], 'deg_fp':x[2]}
    C = 0
    for i in range(3):
        t, y, fp_edges, t_edges = sim.simulate_analytic(model=model,
                                                       model_pars=pars,
                                                       light_profile=light_profile_list[i],
                                                       y0=[0,0],
                                                       n_evals=2)
        C += np.square(datapoints_list[i]-fp_edges[is_timepoint_list[i]]).sum()
    if pars['deg_m'] < pars['deg_fp']:
        return C + np.exp(pars['deg_fp']-pars['deg_m'])
    return C
res = minimize(fun=cost_fun_fit,
                        x0=[1,1,0.4])
fitted_pars = {'sigma':res.x[0], 'deg_m':res.x[1], 'deg_fp':res.x[2]}
print(fitted_pars)

## plot model data together
colors = plt.cm.jet(np.linspace(0.,0.3,3))    
f, axs = plt.subplots(figsize=(12*0.75,8*0.75), ncols=1, nrows=2)
for i in range(3):
    label = None
    if i == 2:
        label='data'
    axs[0].plot(t_measurements_list[i], datapoints_list[i], 'o', color=colors[i], markersize=6, markerfacecolor = colors[i], label=label)
    # leds
    axs[1].plot(leds_list[i][0], leds_list[i][1] + i*2, color=colors[i])
    # simu
    t, y, fp_edges, t_edges = sim.simulate_analytic(model=model,
                                                   model_pars=fitted_pars,
                                                   light_profile=light_profile_list[i],
                                                   y0=[0,0],
                                                   n_evals=31)
    label = None
    if i == 2:
        label='model fit'    
    axs[0].plot(t+t_measurements_list[i][0], y[-1], color=colors[i], label=label)
axs[1].yaxis.set_ticks([])
axs[0].xaxis.set_ticks([])
axs[0].legend(fontsize=12)
axs[1].set(xlim=(-1,16), ylim=(-1,6), ylabel='light intensity', xlabel='Time (hrs)')
axs[0].set(xlim=(-1,16), ylabel='mNeonGreen / FSC (RPU)')
for ax in axs:
    ax.grid()
plt.savefig('plots/EL222-dynamics-fit.pdf')