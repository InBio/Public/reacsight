
## loading cytometry data
import pandas as pd
print('loading data...') 
raw_dfs = {'AF_previous':pd.read_csv('raw_data/inbioreactors-2020-06-23/reactor-5/cells.csv'),
           'mCer_previous':pd.read_csv('raw_data/inbioreactors-2020-06-23/reactor-4/cells.csv'),
           'mNG_previous':pd.read_csv('raw_data/inbioreactors-2020-06-23/reactor-8/cells.csv'),
           'mScI_previous':pd.read_csv('raw_data/inbioreactors-2020-06-23/reactor-6/cells.csv'),
           'AF':pd.read_csv('raw_data/inbioreactors-2020-08-11/reactor-4/cells.csv'),
           'mCer':pd.read_csv('raw_data/inbioreactors-2020-08-11/reactor-6/cells.csv'),
           'mNG':pd.read_csv('raw_data/inbioreactors-2020-08-11/reactor-5/cells.csv'),
           'mScI':pd.read_csv('raw_data/inbioreactors-2020-08-11/reactor-7/cells.csv'),
           '3-colors':pd.read_csv('raw_data/inbioreactors-2020-08-11/reactor-8/cells.csv')}

## size gating
print('size gating (a bit slow, patience...)')
from lib import analysis
gated_dfs = {}
for name,df in raw_dfs.items():
    print(name)
    gated_dfs[name] = analysis.filter_data(data=df, gating_threshold=0.8, doublet_threshold=0.5)
        
## deconvolution
from lib import deconvolution
AF_medians = gated_dfs['AF_previous'].mean()
for name, df in gated_dfs.items():
    df = deconvolution.infer_FP_amounts(AF=AF_medians, data=df, signature_names=['mCerulean', 'mNeonGreen', 'mScarletI'])
    # channel renaming
    for ch_long,ch_short in {"GRN-B-HLin":"GRNB", "GRN-V-HLin":"GRNV", "ORG-G-HLin":"ORGG", "FSC-HLin":"FSC"}.items():
        df[ch_short] = df[ch_long]
    for fp in ['mCerulean', 'mNeonGreen', 'mScarletI', 'GRNB', 'GRNV', 'ORGG']:
        df[f'{fp}_conc_raw'] = df[fp] / df['FSC']
    gated_dfs[name] = df

## conversion in RPU units
# calculate the conversion factors
fp_conc_conversion = {}
fp_short = {'mCerulean':'mCer', 'mNeonGreen':'mNG', 'mScarletI':'mScI'}
for fp,ch in zip(['mCerulean', 'mNeonGreen', 'mScarletI'], ['GRNV','GRNB','ORGG']):
    df = gated_dfs[f'{fp_short[fp]}_previous']
    fp_conc_conversion[fp] = 1. / df[f'{fp}_conc_raw'].median()
    fp_conc_conversion[ch] = 1. / df[f'{ch}_conc_raw'].median()
# apply
for name in ['AF','mCer','mNG','mScI','3-colors', 'AF_previous', 'mCer_previous', 'mNG_previous', 'mScI_previous']:
    df = gated_dfs[name]
    for fp in ['mCerulean', 'mNeonGreen', 'mScarletI', 'GRNV', 'GRNB', 'ORGG']:
        df[f'{fp}_conc'] = df[f'{fp}_conc_raw'] * fp_conc_conversion[fp]
        
## plotting preparation
import matplotlib.pyplot as plt
import numpy as np
plt.rcParams['font.size'] = 15
plt.rcParams['axes.grid'] = True 
plt.rcParams['markers.fillstyle'] = 'none' 
plt.rcParams['lines.markersize'] = 3
plt.rcParams['lines.markeredgewidth'] = 1
plt.rcParams['lines.linewidth'] = 1.25
plt.rcParams['axes.linewidth'] = 1.25
plt.rcParams['figure.figsize'] = (15,10)
strain2color = {'mScI':'red', 'AF':'black', 'mCer':'blue', 'mNG':'green', '3-colors':'purple'}

## plotting deconvolution - 2D scatter plots
n_to_show = 150
f,axs = plt.subplots(ncols=3,nrows=2, figsize=(13,8))
axs = axs.ravel()
strain_name = {'AF':'WT', 'mCer':'pTDH3-mCerulean', 'mNG':'pTDH3-mNeonGreen', 'mScI':'pTDH3-mScarletI', '3-colors':'3 colors'}
for (ch1,ch2),ax in zip([('GRNV', 'GRNB'),('GRNV','ORGG'),('GRNB','ORGG')], axs[:3]):
    for name in ['AF','mCer','mNG','mScI','3-colors']:
        df = gated_dfs[name].sample(n_to_show)
        ax.scatter(df[f'{ch1}_conc'], df[f'{ch2}_conc'], color=strain2color[name], label=strain_name[name])
    ax.set(xlabel=f'{ch1} / FSC (RPU)', ylabel=f'{ch2} / FSC (RPU)')
for (ch1,ch2),ax in zip([('mCerulean', 'mNeonGreen'),('mCerulean','mScarletI'),('mNeonGreen','mScarletI')], axs[3:]):
    for name in ['AF','mCer','mNG','mScI','3-colors']:
        df = gated_dfs[name].sample(n_to_show)
        ax.scatter(df[f'{ch1}_conc'], df[f'{ch2}_conc'], color=strain2color[name], label=name)
    ax.set(xlabel=ch1, ylabel=ch2)
for ax in axs:
    ax.set(xlim=(-0.25,2.5), ylim=(-0.25,2.5))
    ax.grid()
axs[1].legend(fontsize=12, loc='upper center', framealpha=1, markerscale=2)
f.tight_layout()
plt.savefig('plots/deconvolution_2D.pdf')

## plotting stability of distributions
from scipy.stats import gaussian_kde
f,axs = plt.subplots(ncols=3,nrows=1, figsize=(14,4))
to_plot = {'mCerulean':[[],[]], 'mNeonGreen':[[],[]], 'mScarletI':[[],[]]}
for name,ch in zip(['mCer','mNG','mScI'], ['mCerulean', 'mNeonGreen', 'mScarletI']):
    df = gated_dfs[name]
    df_3c = gated_dfs['3-colors']
    i_tp = 0.
    for _,df_tp in df.groupby('time_s'):
        kernel = gaussian_kde(df_tp[f'{ch}_conc'])
        x = np.linspace(-0.1, 3, 1000)
        density = kernel.evaluate(x)
        to_plot[ch][0].append((x,density,strain2color[name],i_tp/50.))
        i_tp += 1.
    i_tp = 0.
    for _,df_tp in df_3c.groupby('time_s'):
        kernel = gaussian_kde(df_tp[f'{ch}_conc'])
        x = np.linspace(-0.1, 3, 1000)
        density = kernel.evaluate(x)
        to_plot[ch][1].append((x,density,strain2color['3-colors'],i_tp/50.))
        i_tp += 1.

for name,ch,ax in zip(['mCer','mNG','mScI'], ['mCerulean', 'mNeonGreen', 'mScarletI'], axs):
    for i,(xx,dd,cc,alpha) in enumerate(to_plot[ch][0]):
        ax.plot(xx, dd, color=cc, alpha=alpha, linewidth=2)
        (xx,dd,cc,alpha) = to_plot[ch][1][i]
        ax.plot(xx, dd, color=cc, alpha=alpha, linewidth=2)
    ax.set(xlabel=f'{ch} / FSC (RPU)', ylabel='density')
    ax.grid()
f.tight_layout()
plt.savefig('plots/constitutive_expression_stability.pdf')