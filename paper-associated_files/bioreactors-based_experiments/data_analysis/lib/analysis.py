import pandas as pd
import numpy as np
import os
import lib.gating as gating

def filter_data(data, gating_threshold, doublet_threshold, fluo_threshold=None):
    if 'gating-metric' not in data:
        ddts = []
        for _,ddt in data.groupby('time_s'):
            ddts.append(gating.compute_ks_FSC_SSC_gating_metric(ddt.copy()))
        data = pd.concat(ddts)
    if 'doublet-metric' not in data:
        ddts = []
        for _,ddt in data.groupby('time_s'):
            ddts.append(gating.compute_doublet_metric(ddt.copy()))
        data = pd.concat(ddts)
    dd = data[np.logical_and(data['gating-metric'] > gating_threshold,
                          data['doublet-metric'] < doublet_threshold)].copy()
    if fluo_threshold is not None:
        fluorophore = fluo_threshold[0]
        thresh = fluo_threshold[1]
        for _,ddt in dd.groupby('time_s'):
            n = len(ddt)
            to_remove_left = ddt.sort_values(f'{fluorophore}')[:int(thresh*n)].index
            to_remove_right = ddt.sort_values(f'{fluorophore}')[int((1-thresh)*n):].index
            dd.drop(to_remove_left, inplace=True)
            dd.drop(to_remove_right, inplace=True)
    return dd
