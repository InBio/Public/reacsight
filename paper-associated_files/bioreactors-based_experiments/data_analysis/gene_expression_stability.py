import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from lib import deconvolution

print('Deconvolution and data processing')

# Here take the data for AF used in deconvolution and for the RPU units    
with open('raw_data/inbioreactors-2020-08-11/reactor-4/cells.csv', 'r') as infile:
    data_AF = pd.read_csv(infile)
    data_AF['GRN-B/FSC'] = data_AF['GRN-B-HLin']/data_AF['FSC-HLin']
    data_AF['GRN-V/FSC'] = data_AF['GRN-V-HLin']/data_AF['FSC-HLin']
    data_AF['ORG-G/FSC'] = data_AF['ORG-G-HLin']/data_AF['FSC-HLin']
    median_AF = data_AF.groupby('time_s').median().reset_index()
    AF_vector = median_AF[-10:].median()
with open('raw_data/inbioreactors-2020-08-11/reactor-5/cells.csv', 'r') as infile:
    data_mNG = pd.read_csv(infile)
    data_mNG['GRN-B/FSC'] = data_mNG['GRN-B-HLin']/data_mNG['FSC-HLin']
    median = data_mNG.groupby('time_s').median().reset_index()
    mNG_RPU = median['GRN-B/FSC'][-10:].median() - AF_vector['GRN-B/FSC']
with open('raw_data/inbioreactors-2020-08-11/reactor-6/cells.csv', 'r') as infile:
    data_mCer = pd.read_csv(infile)
    data_mCer['GRN-V/FSC'] = data_mCer['GRN-V-HLin']/data_mCer['FSC-HLin']
    median = data_mCer.groupby('time_s').median().reset_index()
    mCer_RPU = median['GRN-V/FSC'][-10:].median() - AF_vector['GRN-V/FSC']
with open('raw_data/inbioreactors-2020-08-11/reactor-7/cells.csv', 'r') as infile:
    data_mScI = pd.read_csv(infile)
    data_mScI['ORG-G/FSC'] = data_mScI['ORG-G-HLin']/data_mScI['FSC-HLin'] 
    median = data_mScI.groupby('time_s').median().reset_index()
    mScI_RPU = median['ORG-G/FSC'][-10:].median() - AF_vector['ORG-G/FSC']
    
# Here load the experiments data
reactors = [1, 2, 3, 5]
cells_raw = {}
cells_corrected = {}
cells = {}
POI = {}
POI_median = {}
POI_mean = {}
LED = {}
induction_starts = {}

for reactor in reactors:
    
    path = 'raw_data\\inbioreactors-2022-02-22\\reactor-' + str(reactor)
    
    # Load cytometry data:
    with open(path + '\\cells.csv', 'r') as infile:
        cells_raw[reactor] = pd.read_csv(infile)

    # Compensation for cytometry metrics after changing blue laser intensity on 02/08/2021
    correction_coef_FSC = 1/1.95
    correction_coef_SSC = 1/1.86
    correction_coef_GRN_B = 1/1.72

    corrected_FSC = cells_raw[reactor]['FSC-HLin'] * correction_coef_FSC
    corrected_SSC = cells_raw[reactor]['SSC-HLin'] * correction_coef_SSC
    corrected_GRN_B = cells_raw[reactor]['GRN-B-HLin'] * correction_coef_GRN_B

    cells_corrected[reactor] = cells_raw[reactor].copy()
    cells_corrected[reactor]['FSC-HLin'] = corrected_FSC
    cells_corrected[reactor]['SSC-HLin'] = corrected_SSC
    cells_corrected[reactor]['GRN-B-HLin'] = corrected_GRN_B
    
    # Deconvolution
    deconvol_color = ['mScarletI', 'mNeonGreen', 'mCerulean']

    cells_corrected[reactor] = deconvolution.infer_FP_amounts(AF=AF_vector, data=cells_corrected[reactor], signature_names=deconvol_color)   
    
    # Load LED data
    with open(path + '\\LEDs.csv', 'r') as infile:
        LED[reactor] = pd.read_csv(infile)

    # I use the LED information to set the time 0
    # Computing time in hours units
    LED[reactor]['time_h'] = (LED[reactor]['time_s'] - cells_raw[reactor]['time_s'].iloc[0])/60/60
    
    # Computing when induction starts
    induction_starts_idx = min(LED[reactor][LED[reactor]['intensity'] > 0].index.values)
    induction_starts[reactor] = LED[reactor]['time_h'].iloc[induction_starts_idx] 
    
    # Normalize time to induction
    LED[reactor]['time_norm'] = LED[reactor]['time_h'] - induction_starts[reactor]

    # Gatting based in FSC and SSC
    POI[reactor] = cells_corrected[reactor][(cells_corrected[reactor]['FSC-HLin'] > 1*10**3) & (cells_corrected[reactor]['FSC-HLin'] < 2*10**3)].copy() 

    # Normalize fluorescence by FSC
    POI[reactor]['mCerulean/FSC'] = POI[reactor]['mCerulean']/POI[reactor]['FSC-HLin']
    POI[reactor]['mNeonGreen/FSC'] = POI[reactor]['mNeonGreen']/POI[reactor]['FSC-HLin']
    POI[reactor]['mScarletI/FSC'] = POI[reactor]['mScarletI']/POI[reactor]['FSC-HLin']

    # Computing time in hours units
    POI[reactor]['time_h'] = (POI[reactor]['time_s'] -     POI[reactor]['time_s'].iloc[0])/60/60 

    # Normalize time to induction
    POI[reactor]['time_norm'] = POI[reactor]['time_h'] - induction_starts[reactor]

    # I do remove fluorescence at time 0, because lekeage could happen
    fluo_t0 = np.mean(POI[reactor][(POI[reactor]['time_norm'] < 0) & (POI[reactor]['time_norm'] >= -2)].groupby(['time_h']).median()['mNeonGreen/FSC'])

    POI[reactor]['IF'] = (POI[reactor]['mNeonGreen/FSC'].copy() - fluo_t0)/mNG_RPU

    fluo_t0 = np.mean(POI[reactor][(POI[reactor]['time_norm'] < 0) & (POI[reactor]['time_norm'] >= -2)].groupby(['time_h']).median()['mScarletI/FSC'])            

    POI[reactor]['UPR'] = (POI[reactor]['mScarletI/FSC'].copy() - fluo_t0)/mScI_RPU

    # Compute medians
    POI_median[reactor] = POI[reactor].groupby(['time_h']).median().reset_index()


print('Plotting')

# From now ploting data
# PLOT FIGURE 3D IN LINEAR SCALE

params = {'legend.fontsize': 6,
          'legend.loc' :'lower right',
          'legend.markerscale'   : 1,
          'axes.labelsize' : 6,
          'xtick.labelsize' : 6,
          'ytick.labelsize' : 6,
          'axes.titlesize' : 6,

          'font.family':  'sans-serif',
          'font.style':   'normal',
          'font.variant': 'normal',
          'font.weight':  100,
          'font.stretch': 'normal',
          'font.size':    6.0,
          }

plt.rcParams.update(params)

times_selected = [0, 6, 48, 120]

colors = plt.cm.viridis(np.linspace(0,1,len(times_selected)))

fig, axs = plt.subplots(len(reactors))
fig.set_size_inches(4, 3.15, forward=True)
fig.subplots_adjust(hspace = 0, wspace = 0)

count_reactor = 0

for reactor in reactors:
    
    ax = axs[count_reactor]
            
    x = POI_median[reactor]['time_norm']
    # I do this to show the mean instead of the median
    a = POI[reactor].groupby(['time_norm']).mean().reset_index()
    y = a['IF']

    ax.plot(x, y,     '-o', markerSize = 3,     markeredgewidth = 1, linewidth = 0.5, color = 'limegreen')
    
    ax.set_xlim(-5, 122) # DATA SHOWN FROM time = -5 BECUASE THE FIRST TIME POINT HAS TRACES OF CELLS IN THE DEAD VOLUMES
    ax.set_yscale('log')
    ax.set_ylim(10**-5, 10**2)    
    ax.set_yticks([10**-3, 10**-1, 10**1])
    
    # Create inset of width 30% and height 40% of the parent axes' bounding box
    axins = inset_axes(ax, width="40%", height="70%", loc=1)
    
    count_time = 0

    for time_selected in times_selected:
        
        values = list(abs(POI_median[reactor]['time_norm'] - time_selected))

        t_point = POI_median[reactor]['time_norm'][values.index(np.min(values))]
                
        x = POI_median[reactor]['time_norm'][POI_median[reactor]['time_norm'] == t_point]
    
        a = POI[reactor].groupby(['time_norm']).mean().reset_index()    
        y = a['IF'][a['time_norm'] == t_point]
    
        ax.plot(x, y,         'o', markerSize = 4, markeredgewidth = 0.3, color = colors[times_selected.index(time_selected)], markeredgecolor = 'black')

        x = list(POI[reactor]['IF'][POI[reactor]['time_norm'] == t_point])

        axins.hist(x, bins=np.logspace(np.log10(10**-4),np.log10(10**2), 50),                 color = colors[count_time], alpha = 0.75,
            rasterized=True) 
        
        count_time = count_time + 1

    axins.set_xscale("log")
    axins.set_ylim(0, 800)
    
    axins.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    right=False,         # ticks along the top edge are off
    labelleft=False) # labels along the bottom edge are off

    if reactor != reactors[-1]:    
        ax.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off
        
    else:    
        ax.set_ylabel('mNG levels (RPU)')
        ax.set_xlabel('Time (hrs)')
    
    count_reactor = count_reactor + 1
    
plt.savefig('plots/POI_expression_stability.pdf')   

# PLOT mNG DISTRIBUTION DYNAMICS

plt.rcParams.update(params)

POIs_selected = ['non-sec-mNeon_1', 'mNeon_1', '3x-mNeon_0.3', '3x-mNeon_1']

variable = 'IF'

color = 'palegreen'

times_selected = [0, 6, 48, 120]

colors = plt.cm.viridis(np.linspace(0,1,len(times_selected)))

fig, axs = plt.subplots(len(POIs_selected))
fig.set_size_inches(4, 4, forward=True)
fig.subplots_adjust(hspace = 0, wspace = 0)

count_reactor = 0

for reactor in reactors:
    
    ax = axs[count_reactor]
        
    POI[reactor]['n_cell'] = np.linspace(0,1,len(POI[reactor]))
    t_points = POI[reactor][POI[reactor]['time_norm'] > -4].groupby(['time_norm'])

    for point in list(range(len(t_points))):

        t_point = t_points.get_group(list(t_points.groups)[point])
        
        
        pre_x = t_point['time_norm'][::50]
        
        y = t_point[variable][::50]
        
        x = pre_x * np.ones(len(y)) + np.random.normal(scale=1, size=len(y))

        ax.scatter(x, y, s=2, facecolors='none', c= color, alpha = 1, )
        
    a = POI[reactor][POI[reactor]['time_norm'] > -4].groupby(['time_h']).mean().reset_index()
    y = a[variable]
    x = POI_median[reactor]['time_norm'][POI_median[reactor]['time_norm'] > -4]
    
    ax.plot(x, y, '--', color = 'k')
        
    ax.set_yscale('log')
    ax.set_ylim(10**-5, 10**2)
    ax.set_xlim(-5, 122) # DATA SHOWN FROM time = -5 BECUASE THE FIRST TIME POINT HAS TRACES OF CELLS IN THE DEAD VOLUMES

    if reactor != reactors[-1]:    
        ax.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off
        
    else:
        ax.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=True,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off
        ax.set_xlabel('Time (hrs)')
        ax.set_ylabel('mNG levels (RPU)')

    count_reactor = count_reactor + 1
    
plt.savefig('plots/POI_population.pdf')   

# PLOT mNG DISTRIBUTION DYNAMICS

plt.rcParams.update(params)

POIs_selected = ['non-sec-mNeon_1', 'mNeon_1', '3x-mNeon_0.3', '3x-mNeon_1']

variable = 'UPR'

color = 'salmon'

times_selected = [0, 6, 48, 120]

colors = plt.cm.viridis(np.linspace(0,1,len(times_selected)))

fig, axs = plt.subplots(len(POIs_selected))
fig.set_size_inches(4, 4, forward=True)
fig.subplots_adjust(hspace = 0, wspace = 0)

count_reactor = 0

for reactor in reactors:
    
    ax = axs[count_reactor]
        
    POI[reactor]['n_cell'] = np.linspace(0,1,len(POI[reactor]))
    t_points = POI[reactor][POI[reactor]['time_norm'] > -4].groupby(['time_norm'])

    for point in list(range(len(t_points))):

        t_point = t_points.get_group(list(t_points.groups)[point])
        
        
        pre_x = t_point['time_norm'][::50]
        
        y = t_point[variable][::50]
        
        x = pre_x * np.ones(len(y)) + np.random.normal(scale=1, size=len(y))

        ax.scatter(x, y, s=2, facecolors='none', c= color, alpha = 1, )
        
    a = POI[reactor][POI[reactor]['time_norm'] > -4].groupby(['time_h']).mean().reset_index()
    y = a[variable]
    x = POI_median[reactor]['time_norm'][POI_median[reactor]['time_norm'] > -4]
    
    ax.plot(x, y, '--', color = 'k')
        
    ax.set_yscale('log')
    ax.set_ylim(10**-5, 10**2)
    ax.set_xlim(-5, 122) # DATA SHOWN FROM time = -5 BECUASE THE FIRST TIME POINT HAS TRACES OF CELLS IN THE DEAD VOLUMES

    if reactor != reactors[-1]:    
        ax.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off
        
    else:
        ax.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=True,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off
        ax.set_xlabel('Time (hrs)')
        ax.set_ylabel('mNG levels (RPU)')

    count_reactor = count_reactor + 1
    
plt.savefig('plots/UPR_population.pdf')   





