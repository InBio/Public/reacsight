## loading cytometry and OD data
import pandas as pd
import numpy as np
raw_dfs = {'AF_previous':pd.read_csv('raw_data/inbioreactors-2020-06-23/reactor-5/cells.csv'),
           'mCer_previous':pd.read_csv('raw_data/inbioreactors-2020-06-23/reactor-4/cells.csv'),
           'mScI_previous':pd.read_csv('raw_data/inbioreactors-2020-06-23/reactor-6/cells.csv')}
ods_dfs = {}
for his_conc,folder in zip([4, 20],['inbioreactors-2020-11-17', 'inbioreactors-2020-11-19']):
    for od_setpoint in [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8]:
        rid = int(10*od_setpoint) # that how we mapped od setpoints to reactors
        raw_dfs[f'his_{his_conc}uM_OD_{od_setpoint}'] = pd.read_csv(f'raw_data/{folder}/reactor-{rid}/cells.csv')
        ods_dfs[f'his_{his_conc}uM_OD_{od_setpoint}'] = pd.read_csv(f'raw_data/{folder}/reactor-{rid}/ODs.csv')
t0s = {'his_4uM':1605643303, 'his_20uM':1605801703}        
# remove events with non-positive values for FSC or SSC
for name,df in raw_dfs.items():
    I = np.logical_and(df['FSC-HLin'] > 0, df['SSC-HLin'] > 0)
    if sum(I) < len(df):
        n_removed = len(df) - sum(I)
        raw_dfs[name] = df[I]
        print(f'for {name}, removed {n_removed}/{len(df)} events')
# keep only 12 hours after first time point
for name,df in raw_dfs.items():
    t0 = df.iloc[0]['time_s']
    raw_dfs[name] = df[df['time_s'] < t0 + 12*3600]
    if name in ods_dfs:
        ods_dfs[name] = ods_dfs[name].query(f'time_s < {t0 + 12 * 3600}').copy()
        
## computing size gating metrics
from lib import analysis
gated_dfs = {}
for name,df in raw_dfs.items():
    print(name)
    gated_dfs[name] = analysis.filter_data(data=df, gating_threshold=0, doublet_threshold=np.Inf) # we dont remove anything, but we get the metrics per cell
    
## deconvolution
from lib import deconvolution
AF_medians = gated_dfs['AF_previous'].mean()
for name, df in gated_dfs.items():
    df = deconvolution.infer_FP_amounts(AF=AF_medians, data=df, signature_names=['mCerulean', 'mScarletI'])
    # channel remaining
    for ch_long,ch_short in {"GRN-B-HLin":"GRNB", "GRN-V-HLin":"GRNV", "ORG-G-HLin":"ORGG", "FSC-HLin":"FSC"}.items():
        df[ch_short] = df[ch_long]
    for fp in ['mCerulean', 'mScarletI', 'GRNV', 'ORGG']:
        df[f'{fp}_conc_raw'] = df[fp] / df['FSC']
    gated_dfs[name] = df
    
## conversion in RPU units
# calculate the conversion factors
fp_conc_conversion = {}
fp_short = {'mCerulean':'mCer', 'mScarletI':'mScI'}
for fp,ch in zip(['mCerulean', 'mScarletI'], ['GRNV','ORGG']):
    df = gated_dfs[f'{fp_short[fp]}_previous']
    fp_conc_conversion[fp] = 1. / df[f'{fp}_conc_raw'].median()
    fp_conc_conversion[ch] = 1. / df[f'{ch}_conc_raw'].median()
# apply
for name in gated_dfs:
    df = gated_dfs[name]
    for fp in ['mCerulean', 'mScarletI', 'GRNV', 'ORGG']:
        df[f'{fp}_conc'] = df[f'{fp}_conc_raw'] * fp_conc_conversion[fp]
        
## prepare plotting
import matplotlib.pyplot as plt
plt.rcParams['font.size'] = 10
plt.rcParams['axes.grid'] = True 
plt.rcParams['markers.fillstyle'] = 'none' 
plt.rcParams['lines.markersize'] = 2
plt.rcParams['lines.markeredgewidth'] = 2
plt.rcParams['lines.linewidth'] = 1.5
plt.rcParams['axes.linewidth'] = 1.5
plt.rcParams['figure.figsize'] = (15,10)

## parameters of the analysis
mcer_threshold = 1
gating_pars = (0.5, 0.5)

## illustration single reactor delta mu computation (his 4, od 0.4)
f, (ax1,ax2) = plt.subplots(ncols=1, nrows=2, figsize=(6,6))
reactor_str = f'his_4uM_OD_0.4'
start_delay = 4
duration = 12
df = gated_dfs[reactor_str]
od = ods_dfs[reactor_str]
tp_start = od.iloc[0]['time_s'] + start_delay*3600
tp_end = tp_start + duration*3600
# compute the ratios
ratios,tps = [],[]
for tp,cells in df.groupby('time_s'):
    cells = cells[np.logical_and(
        cells['gating-metric'] > gating_pars[0],
        cells['doublet-metric'] < gating_pars[1])]
    ratio = sum(cells['mCerulean_conc'] > mcer_threshold) / sum(cells['mCerulean_conc'] <= mcer_threshold)
    ratios.append(ratio)
    tps.append(tp)
ratios = pd.DataFrame.from_dict({'tp':tps,'ratio':ratios})
# select ratios for analysis
ratios_kept = ratios.query(f'tp >= {tp_start} and tp <= {tp_end}').copy()
for r in [ratios, ratios_kept]:
    r['tp_rel_hrs'] = (r['tp']-tp_start)/3600
# compute delta mu
pfit = np.polyfit(ratios_kept['tp_rel_hrs'], np.log(ratios_kept['ratio']), 1)
# plot ratio and delta mu
ax2.plot(ratios['tp_rel_hrs'], np.log(ratios['ratio']), 'ko', markersize=10, markerfacecolor='k')
ax2.plot(ratios_kept['tp_rel_hrs'], 
            np.polyval(pfit,ratios_kept['tp_rel_hrs']),'b', linewidth=2)
# plot ODs
od_before = od.query(f'time_s < {tp_start}') 
od_select = od.query(f'time_s >= {tp_start} and time_s <= {tp_end}')
ax1.plot((od_before['time_s']-tp_start)/3600, od_before['OD'], 'k')
ax1.plot((od_select['time_s']-tp_start)/3600, od_select['OD'], 'b')
for ax in (ax1,ax2):
    ax.grid()
    ax.xaxis.label.set_size(20)
    ax.yaxis.label.set_size(20)
    ax.tick_params(axis='both', which='major', labelsize=16)
    ax.tick_params(axis='both', which='minor', labelsize=16)
ax1.set(ylabel='OD')
ax2.set(xlabel='Time (hrs)', ylabel='log(WT/mutant)')
f.tight_layout()
plt.savefig('plots/histidine_scarcity_example_delta_mu.pdf')

## main analysis function
# his 4 or 20 uM
# mcer_threshold for genotyping
# gating_pars = (size_threshold, doublet_threshold)
def analyze_his_conc(his, mcer_treshold, gating_pars, start_delay, duration):
    f, axs = plt.subplots(ncols=6, nrows=9, figsize=(15,20))
    delta_mus, uprs_proto, uprs_auxo = [],[],[]
    ods = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]
    for i_od,od in enumerate(ods):
        reactor_str = f'his_{his}uM_OD_{od}'
        df = gated_dfs[reactor_str]
        od = ods_dfs[reactor_str]
        tp_start = od.iloc[0]['time_s'] + start_delay*3600
        tp_end = tp_start + duration*3600
        # compute the ratios
        ratios,tps = [],[]
        for tp,cells in df.groupby('time_s'):
            cells = cells[np.logical_and(
                cells['gating-metric'] > gating_pars[0],
                cells['doublet-metric'] < gating_pars[1])]
            ratio = sum(cells['mCerulean_conc'] > mcer_threshold) / sum(cells['mCerulean_conc'] <= mcer_threshold)
            ratios.append(ratio)
            tps.append(tp)
        ratios = pd.DataFrame.from_dict({'tp':tps,'ratio':ratios})
        # select ratios for analysis
        ratios_kept = ratios.query(f'tp >= {tp_start} and tp <= {tp_end}').copy()
        for r in [ratios, ratios_kept]:
            r['tp_rel_hrs'] = (r['tp']-tp_start)/3600
        # compute delta mu
        pfit = np.polyfit(ratios_kept['tp_rel_hrs'], np.log(ratios_kept['ratio']), 1)
        delta_mus.append(pfit[0])
        # plot ratio and delta mu
        axs[i_od,1].plot(ratios['tp_rel_hrs'], np.log(ratios['ratio']), 'ko')
        axs[i_od,1].plot(ratios_kept['tp_rel_hrs'], 
                    np.polyval(pfit,ratios_kept['tp_rel_hrs']),'r')
        # plot ODs
        axs[i_od,0].plot((od['time_s']-tp_start)/3600, od['OD'], 'g')
        for tp in df['time_s'].unique():
            if tp in list(ratios_kept['tp']):
                axs[i_od,0].plot([(tp-tp_start)/3600]*2, [0,1], 'k')
            else:
                axs[i_od,0].plot([(tp-tp_start)/3600]*2, [0,1], '--k')
        # plot single cells
        for i_cells,(_,point) in enumerate(ratios_kept.iterrows()):
            tp = point['tp']
            cells = df.query(f'time_s == {tp}')
            cells = cells[np.logical_and(
                cells['gating-metric'] > gating_pars[0],
                cells['doublet-metric'] < gating_pars[1])]
            proto = cells.query(f'mCerulean_conc > {mcer_threshold}')
            auxo =  cells.query(f'mCerulean_conc <= {mcer_threshold}')
            axs[i_od,i_cells+2].scatter(proto['mCerulean_conc'], proto['mScarletI_conc'], s=5, c='blue', label='his +')
            axs[i_od,i_cells+2].scatter(auxo['mCerulean_conc'], auxo['mScarletI_conc'], s=5, c='black', label='his -')
            axs[i_od,i_cells+2].plot([mcer_threshold]*2, [-10,10], '--k')
            axs[i_od,i_cells+2].set(xlim=(-0.5, 8), ylim=(-0.02, 0.5))
            # plot the upr
            if i_cells == 0:
                axs[i_od, -1].scatter(np.arange(len(proto)), proto['mScarletI_conc'], s=5, c='blue')
                axs[i_od, -1].scatter(len(proto)+np.arange(len(auxo)), auxo['mScarletI_conc'], s=5, c='black')
                axs[i_od, -1].set(ylim=(-0.02, 0.5))
                uprs_proto.append(proto['mScarletI_conc'].median())
                uprs_auxo.append(auxo['mScarletI_conc'].median())
    # plot curve of all delta mus
    axs[-1,0].plot(ods, delta_mus, '-ko')
    axs[-1,0].set(ylim=(0., 0.8))
    # styling
    for ax in axs.ravel():
        ax.grid()
    f.tight_layout()
    plt.close('all')
    return ods, delta_mus, uprs_proto, uprs_auxo

## doing the analysis
result_4uM = analyze_his_conc(his=4, mcer_treshold=1, gating_pars=(0.5,0.5), start_delay=3, duration=7)
result_20uM = analyze_his_conc(his=20, mcer_treshold=1, gating_pars=(0.5,0.5), start_delay=9, duration=20)

## plotting results: delta mu
f, ax = plt.subplots(ncols=1, nrows=1, figsize=(6.5,4))
ods, delta_mus, uprs_proto, uprs_auxo = result_4uM
ax.plot(ods, delta_mus, 'o', color='lightgreen', markersize=15, markerfacecolor='lightgreen')
ods, delta_mus, uprs_proto, uprs_auxo = result_20uM
ax.plot(ods, delta_mus, 's', color='darkgreen', markersize=15, markerfacecolor='darkgreen')
ax.grid()
ax.xaxis.label.set_size(20)
ax.yaxis.label.set_size(20)
ax.tick_params(axis='both', which='major', labelsize=16)
ax.tick_params(axis='both', which='minor', labelsize=16)
ax.set(xlabel='OD setpoint', ylabel='fitness cost', ylim=(-0.05, 0.8))
f.tight_layout()
plt.savefig('plots/histidine_scarcity_delta_mu_vs_OD.pdf')

## plotting results: upr 20 uM
color_WT = np.array((25, 174, 255))/255
f, ax = plt.subplots(ncols=1, nrows=1, figsize=(6.5,4))
ods, delta_mus, uprs_proto, uprs_auxo = result_20uM
ax.plot(ods, uprs_proto, 's', color=color_WT, markersize=15, markerfacecolor=color_WT)
ax.plot(ods, uprs_auxo, 's', color='black', markersize=15, markerfacecolor='black')
ax.grid()
ax.xaxis.label.set_size(20)
ax.yaxis.label.set_size(20)
ax.tick_params(axis='both', which='major', labelsize=16)
ax.tick_params(axis='both', which='minor', labelsize=16)
ax.set(xlabel='OD setpoint', ylabel='UPR reporter level', ylim=(0, 0.2))
f.tight_layout()
plt.savefig('plots/histidine_scarcity_20uM_upr.pdf')

## plotting results: upr 4 uM
f, ax = plt.subplots(ncols=1, nrows=1, figsize=(6.5,4))
ods, delta_mus, uprs_proto, uprs_auxo = result_4uM
ax.plot(ods, uprs_proto, 'o', color=color_WT, markersize=15, markerfacecolor=color_WT)
ax.plot(ods, uprs_auxo, 'o', color='black', markersize=15, markerfacecolor='black')
ax.grid()
ax.xaxis.label.set_size(20)
ax.yaxis.label.set_size(20)
ax.tick_params(axis='both', which='major', labelsize=16)
ax.tick_params(axis='both', which='minor', labelsize=16)
ax.set(xlabel='OD setpoint', ylabel='UPR reporter level', ylim=(0, 0.2))
f.tight_layout()
plt.savefig('plots/histidine_scarcity_4uM_upr.pdf')
