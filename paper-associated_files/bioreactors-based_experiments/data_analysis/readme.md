
## Generating figure plots of the paper from raw data

### Dependencies and compatible environments

* Python 3.6+
* pandas 1.0.1+
* scipy 1.4.1+
* numpy 1.19.2+
* matplotlib 3.1.3+

Tested on several environments, including miniconda-managed Python distributions on Windows 7 and Windows 10 computers.

### Instructions

* Get the data from [Zenodo](https://doi.org/10.5281/zenodo.4776010), extract in the `raw_data` folder

* run scripts from this folder:
    + `python metrology_and_deconvolution.py`
        - Figures 2b, and 2c
    + `python EL222_data_and_model_fitting.py`
		- Figures 2d, and 3a
    + `python EL222_model_predictive_control.py`
        - Figure 3c
    + `gene_expression_stability.py`
        - Figures 3d, Supplementary Fig 11, and Supplementary Fig 12 
    + `python histidine_scarcity.py`
        - Figure 4b
    + `python two-strain_control.py`
        - Figure 4c

Can take typically 1-3 minutes per script on a standard laptop, gating of cytometry data being the main speed bottleneck.

### Complete examples of data generation for the `gene_expression_stability` experiment can be found at ...\bioreactors-based_experiments\data_generation\