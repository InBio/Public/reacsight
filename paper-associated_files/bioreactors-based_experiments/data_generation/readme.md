# Data generation 

```gene_expression_stability.ipynb``` --- Jupyter notebook controlling the experiment of 'gene_expression_stability' specific for our in-house bioreactors array (Fig 3d).

```chi.bio_experiment.ipynb``` --- Jupyter notebook controlling the experiment using Chi.Bio reactors (Supplementary Fig 6). 

Each of Jupyter notebooks: 
- launches bioreactors programs
- collects and OD and cytometer measurements, and controls experimental conditions
- controls the pipetting robot 
- plots experimental data in real-time