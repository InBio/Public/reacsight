# Bioreactors based applications 

## About

This part of repository hosts content related to the ReacSight strategy to connect a bioreactors array (in-house bioreactors, or Chi.Bio) with a pipetting robot OT-2 (api v1) and with a cytometer (Guava EasyCyte 14HT, Luminex) to do the experiments shown in Figures 3d, and Supplementary Fig 6. 

## Structure 

This folder contains two parts: 
- data_generation: script of the experiments 
- data_analysis: code that generated the figures 

### Data generation 

The directory includes two Jupyter Notebooks showing the interface used to control experiments in bioreactors platform, for bothm the in-house bioreactors array and the Chi.Bio. The connection between the different devices (arduinos, pipetting robot and cytometer) is done by using the presented in ...\ReacSight_strategy

### Data analysis 

This folder contains scripts that generate figures. 