# Data generation 

```flaskr.py``` --- script for establishing connection between controller and pipetting robot and for definition of used labware and robot commands. 

```20220408_turbidostat-like.ipynb``` --- Jupyter notebook controlling the experiment of 'turbidostat mode' (Fig 5b).

```20220208_OD-based_repeated_treatment.ipynb``` --- Jupyter notebook controlling the experiment of second dose of antibiotic treatment (Fig 5c). 

Each of Jupyter notebooks: 
- launches platereader programs
- collects and analyses OD measurements
- if needed opens the drawer to put the plate out or closes it 
- controls the pipetting robot 