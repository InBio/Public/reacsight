from flask import Flask, request, redirect, url_for, render_template
import opentrons as opentrons
import numpy as np
import json

opentrons.robot.connect()
opentrons.robot.discover_modules()
opentrons.robot.reset()
opentrons.robot.home()

json_96 = json.load(open('nest_96_wellplate_200ul_flat.json'))

json_trail = json.load(open('nest_96_wellplate_200ul_flat.json'))
json_trail["cornerOffsetFromSlot"] = {'x': -37, 'y': 26, 'z': 60}
trail = opentrons.labware.robot.add_container_by_definition(json_trail,'7',share=True)

waste_funnel = opentrons.containers.load('nest_1_reservoir_195ml', '11', share=True)

tiprack1 = opentrons.labware.load('opentrons-tiprack-300ul', '1')
tiprack2 = opentrons.labware.load('opentrons-tiprack-300ul', '2')
tiprack3 = opentrons.labware.load('opentrons-tiprack-300ul', '3')
tiprack4 = opentrons.labware.load('opentrons-tiprack-300ul', '4')

module = opentrons.modules.load('tempdeck', '9')

plate_temp = opentrons.containers.load('nest_96_wellplate_2ml_deep', '9', share=True) 

labware_dictionary = {'trail':trail,'waste_funnel':waste_funnel,'plate_temp':plate_temp}

pipette = opentrons.instruments.P300_Multi_GEN2(mount='left', tip_racks=[tiprack1, tiprack2, tiprack3, tiprack4])
#pipette2 = opentrons.instruments.P300_Single_GEN2(mount='right', tip_racks=tiprack2)

app = Flask(__name__)

@app.route('/')
def nothin(): #does nothing
    return('all good buddy')

@app.route('/tips/pick_up')
def tips_pick_up():
    pipette.pick_up_tip(presses = 1)
    return('tips picked')
@app.route('/tips/drop')
def tips_drop(): #should return tips in their original tiprack
    pipette.drop_tip()
    return('tips dropped')
@app.route('/tips/return')
def tips_return(): #drop tips in the bin
    pipette.return_tip()
    return('tips returned')
@app.route('/tips/reset')
def tips_reset(): # to do when refilling all tipracks
    pipette.reset()
    return('tips tracking reset')

@app.route('/transfer/<int:volume>/<string:from_labware>/<int:from_col_number>/<string:to_labware>/<int:to_col_number>')
def transfer(volume,from_labware,from_col_number,to_labware,to_col_number): #transfer volume from a labware's column to another, picks up tips if needed
    if pipette.has_tip == False:
            pipette.pick_up_tip(presses = 1)
    labware1 = labware_dictionary[from_labware]
    labware2 = labware_dictionary[to_labware]
    pipette.transfer(
            volume,
            labware1.columns(str(from_col_number)),
            labware2.columns(str(to_col_number)),
            new_tip='never',
            mix_after=(3, 100))
    gotobin()
    pipette.blow_out()
    return('done gotcha')

@app.route('/dilute/<int:volume_in>/<string:from_labware>/<int:from_col_number>/<string:to_labware>/<int:to_col_number>/<int:volume_out>')
def dilute(volume_in,from_labware,from_col_number,to_labware,to_col_number, volume_out): #transfer volume from a labware's column to another, picks up tips if needed
    if pipette.has_tip == False:
            pipette.pick_up_tip(presses = 1)
    labware1 = labware_dictionary[from_labware]
    labware2 = labware_dictionary[to_labware]
    pipette.transfer(
                int(volume_in),
                labware1.columns(str(from_col_number)),
                labware2.columns(str(to_col_number)),
                new_tip='never',
                mix_after=(3, 100))
    pipette.aspirate(int(volume_out), labware2.columns(str(to_col_number)))
#     pipette.move_to(waste_funnel.columns('1'))
    gotobin()
    pipette.blow_out()
    pipette.drop_tip()
    gohome()
    return('done gotcha')


@app.route('/transfer_well/<int:volume>/<string:from_labware>/<string:from_well>/<string:to_labware>/<string:to_well>')
def transfer_well(volume,from_labware,from_well,to_labware,to_well): #transfer volume from a labware's well to another, picks up tips if needed
    if pipette2.has_tip == False:
            pipette2.pick_up_tip(presses = 1)
    labware1 = labware_dictionary[from_labware]
    labware2 = labware_dictionary[to_labware]
    pipette2.transfer(
            volume,
            labware1.well(from_well),
            labware2.well(to_well),
            new_tip='never',
            mix_after=(3, 100))
    gotobin()
    pipette.blow_out()
    return('done gotcha')

@app.route('/home')
def gohome(): # sends robot to home position 
    opentrons.robot.home()
    return('success?')

@app.route('/temp/<int:temperature>')
def temp(temperature): #set temperature on the temperature module 
    module.set_temperature(int(temperature))
    return('success?')

@app.route('/gotobin')
def gotobin(): #go to the liquid trash bin 
    pipette.move_to(waste_funnel.columns('1'))
    return('success trash')

@app.route('/move_to/<string:labware>/<int:col_number>')
def moveto(labware,col_number):  # move to a column in a labware 
    pipette.move_to(labware_dictionary[labware].columns(str(col_number)))
    return('done gotcha')