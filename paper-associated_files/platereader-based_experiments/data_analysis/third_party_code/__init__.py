# Copyright (C) Virgile Andreani (Inria, Institut Pasteur) <virgile.andreani@normalesup.org>


from pathlib import Path
import numpy as np
from matplotlib import rc
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import scipy.stats as stats
import scipy.signal as signal
import pandas as pd

class Experiments:
    def __init__(self, exps_list=None):
        self.exps = [] if not exps_list else exps_list

    def __iter__(self):
        for exp in self.exps:
            yield exp

    def add(self, exp):
        self.exps.append(exp)

    def plot(self, ax, *args, p=None, **kwargs):
        for exp in self:
            exp.plotod(ax[0])
            if exp.timescfus is not None:
                exp.plotn(ax[1], 'o')
        if p is not None:
            plt_reset_colors(ax[0])
            plt_reset_colors(ax[1])
            for exp in self:
                exp.plotod_sim(p, ax[0], '--')
                exp.plotn_sim(p, ax[1], '--')
                exp.plotl_sim(p, ax[2], '--')
        ax[0].set_yscale('log')
        ax[1].set_yscale('log')
        ax[2].set_yscale('log')
        ax[1].set_ylim(0.1, None)
        ax[0].set_ylabel('Optical Density')
        ax[1].set_ylabel('Cell Number')
        ax[2].set_ylabel('Cell Length')


class Experiment:
    def __init__(self, a0, od0, s0, times, ods,
                 timescfus=None, cfus=None, stdcfus=None,
                 timesinjs=None, volsinjs=None, concinj=None,
                 v0=200):
        times = np.array(times)
        ods = np.array(ods)
        self.a0 = a0
        self.od0 = od0
        self.s0 = s0
        self.times = times[ods > 0]
        self.ods = ods[ods > 0]
        self.lods = np.log(self.ods)
        self.stdods = 0.02*self.ods + 1e-4
        self.stdlods = self.stdods / self.ods
        self.m_dil = 0.05
        self.timescfus = timescfus
        self.cfus = np.array(cfus)
        self.stdcfus = np.array(stdcfus)
        if timescfus is not None:
            self.lcfus = np.log(self.cfus)
            self.stdlcfus = self.stdcfus / self.cfus
        self.timesinjs = timesinjs
        self.volsinjs = volsinjs
        self.concinj = concinj
        self.v0 = v0


def smooth(ys):
    threshold = 0.002
    peaks = ((ys[1:-1] - ys[:-2]) > threshold) & ((ys[1:-1] - ys[2:]) > threshold)
    avgs = (ys[:-2] + ys[2:]) / 2
    ys2 = np.array(ys)
    ys2[1:-1][peaks] = avgs[peaks]
    return signal.savgol_filter(ys2, 15, 2)


def remove_background(ods, od0=0.05/100, avgrange=None,
                      factor=1.5, peaks=True):
    if not avgrange:
        avgrange = range(3, 8)
    shifted = ods - ods[avgrange].mean(axis=0) + od0 * factor
    if peaks:
        return smooth(shifted)
    return shifted


def read_excel(name):
    data = np.array(pd.read_excel(name))
    start = np.where(data[:, 0] == 'Cycle Nr.')[0][0] + 1
    stop = np.where(data[:, 0] == 'End Time')[0][0] - 1
    times = np.array(data[start:stop, 1], dtype=np.float)
    plates = np.array(data[start:stop, 3:], dtype=np.float)
    return times / 3600, plates


def read_abs(path='.'):
    datas = []
    for p in sorted(Path(path).glob('20*_abs.csv')):
        with open(p, 'r') as f:
            d = np.loadtxt(f, delimiter=',')[::3]
        datas.append(d)
    data = np.concatenate(datas)
    times = (data[:, 0] - data[0, 0]) / 3600
    plates = data[:, 1:]
    return times, plates
