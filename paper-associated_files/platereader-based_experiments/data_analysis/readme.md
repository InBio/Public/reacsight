# Data analysis 

## External dependencies 

This code uses a third party code written by Virgile Andreani (Inria & Institut Pasteur, <virgile.andreani@normalesup.org> ) (https://doi.org/10.1101/2021.07.16.452741; https://gitlab.inria.fr/InBio/Public/esbl-escape)


In addition this code requires following packages: 

```
numpy 
matplotlib
pandas
scipy
``` 


## Data 
In the raw data presented on Zenodo (https://doi.org/10.5281/zenodo.4776009)  'platereader-2022-04-08' corresponds to raw data for the turbidostat experiment (Fig 5b) and 'platereader-2022-02-09' corresponds to the second treatment experiment (Fig 5c). 


## Running the code 

First check if you have python 3.7+ and all the dependencies. 

Further run: 
```
python 20220408_turbidostat-like_results.py
python 20220208_OD-based_repeated_treatment_results.py
``` 
to get the figures 5b and 5c respectively. 
