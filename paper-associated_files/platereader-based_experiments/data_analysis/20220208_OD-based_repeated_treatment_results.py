#This code used code written by Virgile Andreani (Inria & Institut Pasteur, <virgile.andreani@normalesup.org> ) (https://doi.org/10.1101/2021.07.16.452741; https://gitlab.inria.fr/InBio/Public/esbl-escape)
import third_party_code as esbl1

import glob
import os
import numpy as np 
import pandas as pd 
from pathlib import Path
import matplotlib.pyplot as plt 
from collections import OrderedDict


# Set path to the raw data: here we assume that there are in a subfolder platereader-2022-02-09
path = os.getcwd()
path = os.path.join(path, 'raw_data/platereader-2022-02-09')
well_names = np.array([L + str(i) for L in "ABCDEFGH" for i in range(1, 13)])


def get_tecan_results_file(path=path): 
    file_type = '*.xlsx'
    files = glob.glob(path+ '/' + file_type)
    last_file = max(files, key=os.path.getctime)
    return last_file

def read_excel(name): 
    data = np.array(pd.read_excel(name, engine='openpyxl'))
    start = np.where(data[:, 0] == 'Cycle Nr.')[0][0] + 1
    stop = np.where(data[:, 0] == 'End Time')[0][0] - 1
    times = np.array(data[start:stop, 1], dtype=float)
    plates = np.array(data[start:stop, 3:], dtype=float)
    return times / 3600, plates


def get_all_results_excel(path = path): 
    datas = []
    times = [] 
    start = 0
    i = 0
    for p in sorted(Path(path).glob('Viktoriia*2022*kinetic*.xlsx')):
        # print(p)
        t, d = read_excel(p)
        datas.append(d)
        name = str(p).split('_')
        start_time, _ = (name[-1]).split('.')
        start_time = int(start_time[:2]) * 3600 + int(start_time[2:4]) * 60 + int(start_time[4:6]) - start  
        if i == 0: 
            start = start_time
            start_time = 0
        elif start_time < 0: 
            start_time += 24 * 3600 
        times.append(t + start_time / 3600) 
        i+=1
    data = np.concatenate(datas)
    times = np.concatenate(times)
    return times, data       


### Get experimental results ### 
time, data = get_all_results_excel()
experiments = []
for i in range(8 * 4):
    experiments.append(esbl1.Experiments())
media = np.array(['1 g/l glucose', '1 g/l glucose + 0.125 mg/l ctx', '1 g/l glucose + 0.25 mg/l ctx', '1 g/l glucose + 0.5 mg/l ctx', 
                  '1 g/l glucose + 1 mg/l ctx', '1 g/l glucose + 2 mg/l ctx', '1 g/l glucose + 4 mg/l ctx', '1 g/l glucose + 16 mg/l ctx'])
od_empty = np.median(data[np.where(np.isfinite(data[:, 12 * 3 + 1]))[0], 12 * 3 + 1])
for i in range(1, 9):
    for k in range(4, 8): 
        #antibiotic concentration
        if i <= 2: a0 = 0
        elif k < 6: a0 = 2**((i - 3) // 2 )
        elif k == 6:  a0 = 2**(i % 3)
        od0 = 5 * 10 ** (-4)
        ods = data[:, 12 * k + i] - od_empty
        experiments[(k - 4) * 8 + i - 1].add(esbl1.Experiment(a0, od0, 1, time, ods))
    

### PLOT ### 
f3, ax3 = plt.subplots(1, 3, sharex=True, figsize = (15, 6))

concentrations = [str(2 ** (x - 3)) + " mg/l" for x in range(0, 6)]
concentrations = np.append(np.array(['0']), concentrations)
concentrations = np.append(concentrations, '16 mg/l') 

colors = plt.cm.tab20(np.linspace(0, 1, concentrations.size))

treatment_time_1 = np.array([])
treatment_time_2 = np.array([])

treatment = pd.DataFrame(data = [], columns = ["media", "time of treatment", "OD of treatment"])


for k in range(32):
    for exp in experiments[k].exps:
        ods1 = exp.ods[np.logical_and(exp.times < 1.5, exp.ods < 3 * 10**(-3))]
        times1 = exp.times[np.logical_and(exp.times < 1.5, exp.ods < 3 * 10**(-3))]
        ods2 = exp.ods[exp.times >= 1.5]
        times2 = exp.times[exp.times >= 1.5]
        
        ods = np.append(ods1, ods2) 
        times = np.append(times1, times2) 
        treatment_time_1 = np.append(treatment_time_1, exp.times[np.where(np.diff(exp.times) > 7/60)[0][-2] + 1]) 
        treatment_time_2 = np.append(treatment_time_2, exp.times[np.where(np.diff(exp.times) > 7/60)[0][-1] + 1]) 
        OD_treatment_1 = exp.ods[np.where(np.diff(exp.times) > 7/60)[0][-2] + 1] 
        OD_treatment_2 = exp.ods[np.where(np.diff(exp.times) > 7/60)[0][-1] + 1] 
        ax3[(k // 8 - 1 * ( k // 8 > 1) + 1) % 3 ].scatter(times, esbl1.smooth(ods),  label = concentrations[k % 8], color = colors[k % 8 ], s = 4)
        

ax3[1].axhline(y = 0.0025, c = 'red', label = 'treatment', linestyle = '--') 
ax3[2].axhline(y = 0.005, c = 'red', label = 'treatment', linestyle = '--')
ax3[0].axhline(y = 0.005, c = 'blue', label = 'fresh media', linestyle = '--') 
ax3[1].set_title("Treatment with antibiotic (16 mg/l) \n when OD > 0.0025", fontsize = 14, fontname="Arial") 
ax3[2].set_title("Treatment with antibiotic (16 mg/l) \n when OD > 0.005", fontsize = 14, fontname="Arial") 
ax3[0].set_title(" \"Treatment\" with fresh media \n when OD > 0.005", fontsize = 14, fontname="Arial") 
for i in range(ax3.size): 
    ax3[i].set_yscale('log')
    ax3[i].set_ylim(2 * 10**(-4), 0.3)
    ax3[i].set_xlim(0, 15)
    ax3[i].tick_params(axis='both', which='both', labelsize=14)
    ax3[i].set_ylabel('OD 600', fontsize = 14, fontname="Arial")
    ax3[i].set_xlabel('time (h)', fontsize = 14, fontname="Arial")
    handles, labels = ax3[i].get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    ax3[i].legend(by_label.values(), by_label.keys(), loc = 'lower right',  markerscale=6, fontsize = 12, ncol = 2)
    
f3.tight_layout()
f3.savefig('plots/fig5c.pdf', format='pdf', dpi=500)

plt.show() 
