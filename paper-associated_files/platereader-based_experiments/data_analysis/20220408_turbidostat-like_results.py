#This code used code written by Virgile Andreani (Inria & Institut Pasteur, <virgile.andreani@normalesup.org> ) (https://doi.org/10.1101/2021.07.16.452741; https://gitlab.inria.fr/InBio/Public/esbl-escape)
import third_party_code as esbl1

import glob
import os
import numpy as np 
import pandas as pd 
from pathlib import Path
import matplotlib.pyplot as plt 
from collections import OrderedDict
import time

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

path = os.getcwd()
path = os.path.join(path, 'raw_data/platereader-2022-04-08')
well_names = np.array([L + str(i) for L in "ABCDEFGH" for i in range(1, 13)])


def get_tecan_results_file(path=path):
    file_type = '*.xlsx'
    files = glob.glob(path+ '/' + file_type)
    last_file = max(files, key=os.path.getctime)
    return last_file

def read_excel_rs(name):
    try:
        data = np.array(pd.read_excel(name, engine='openpyxl'))
        start = np.where(data[:, 0] == 'Cycle Nr.')[0][0] + 1
        stop = np.where(data[:, 0] == 'End Time')[0][0] - 1
        if 'AutoFl' in data[:, 0] :
            stop = np.where(data[:, 0] == 'AutoFl')[0][0] - 1
        # print(name, start, stop)
        times = np.array(data[start:stop, 1], dtype=float)
        plates = np.array(data[start:stop, 3:], dtype=float)
        if times.size > 1:
            return times / 3600, plates
        else:
            return np.array([]), np.array([])
    except:
        return np.array([]), np.array([])

def read_excel_rs_fluo(name):
    try:
        data = np.array(pd.read_excel(name, engine='openpyxl'))
        start = np.where(data[:, 0] == 'Cycle Nr.')[0][0] + 1
        stop = np.where(data[:, 0] == 'End Time')[0][0] - 1
        if 'AutoFl' in data[:, 0] :
            stop = np.where(data[:, 0] == 'AutoFl')[0][0] - 1
            start_fluo = stop + 2
            stopt_fluo = np.where(data[:, 0] == 'End Time')[0][0] - 1
        # print(name, start, stop)
        times = np.array(data[start:stop, 1], dtype=float)
        plates = np.array(data[start:stop, 3:], dtype=float)
        if 'AutoFl' in data[:, 0] :
            times_fl = np.array(data[start_fluo:stopt_fluo, 1], dtype=float)
            plates_fl = np.array(data[start_fluo:stopt_fluo, 3:], dtype=float)
        if times.size > 1:
            return times / 3600, plates, plates_fl
        else:
            return np.array([]), np.array([]), np.array([])
    except:
        return np.array([]), np.array([]),np.array([])

def get_all_results_excel(path = path, pattern = 'Viktoriia*2022*kinetic*.xlsx', fluorescence = False):
    datas = []
    times = []
    fluos = []
    start = 0
    i = 0
    last_start_time = 0
    fl = np.array([])
    for p in sorted(Path(path).glob(pattern)):
        if not fluorescence:
            t, d = read_excel_rs(p)
        else:
            t, d, fl = read_excel_rs_fluo(p)
        if t.size > 0:
            datas.append(d)
            if fl.size > 0:
                fluos.append(fl)
            name = str(p).split('_')
            start_time, _ = (name[-1]).split('.')
            start_time = int(start_time[:2]) * 3600 + int(start_time[2:4]) * 60 + int(start_time[4:6]) - start
            if i == 0:
                start = start_time
                start_time = 0
            elif start_time < 0:
                start_time += 24 * 3600
            elif start_time < last_start_time:
                start_time += 24 * 3600
            times.append(t + start_time / 3600)
            last_start_time = start_time
            i+=1
    data = np.concatenate(datas)
    times = np.concatenate(times)
    if fluorescence:
        fluo = np.concatenate(fluos)
        return times, data, fluo
    return times, data
     
    
def growth_rate (ods, times, start, end):
    od_1 = np.array(ods[np.nonzero(np.where((times > start) & (times < end), 1, 0))])
    time_1 = np.array(times[np.nonzero(np.where((times > start) & (times < end), 1, 0))])
    if od_1.size > 0:
        log_OD = np.log(od_1)
        slope, intercept, r_value, p_value, std_err = stats.linregress(time_1[np.isfinite(log_OD)], log_OD[np.isfinite(log_OD)])
        return slope
    else:
        return 0
    

    
### Get experimental results ### 

time, data = get_all_results_excel(path) # put path to folder with xlsx files here 


experiments = []
for i in range(4):
    experiments.append(esbl1.Experiments())

media = np.array(['1 g/L glucose', '1 g/L glucose + 0.2 % casamino acids', '1 g/L glucose', '1 g/L glucose + 0.2 % casamino acids'])
antibiotic = np.array(['0', '0.25 mg/L', '0.5 mg/L', '1 mg/L'])

od_empty = np.median(data[np.where(np.isfinite(data[:, 12 * 3 + 1]))[0], 12 * 3 + 1])

for k in range(4, 8): 
    for i in range(1, 9):
        #antibiotic concentration
        if i <= 2:
            a0 = 0
        else: 
            a0 = 2 ** ((i - 3) // 2 - 2)             
        od0 = 5 * 10 ** (-4)
        ods = esbl1.remove_background(data[:, 12 * k + i], od0=od0, peaks = False)
        experiments[(k - 4)].add(esbl1.Experiment(a0, od0, 1, time, ods))

with open(os.path.join(path, '2022-04-08_dilution_times.txt'), 'r') as f: 
    input_dil = f.read() 
input_dil = list(input_dil[2:-1].replace(']', '').split('['))
dilution_times = [] 
for i in range(len(input_dil)): 
    a = input_dil[i].split(', ')
    if i < len(input_dil) - 1: 
        a = np.array(a[:-1], dtype = float)
    else: 
        a = a = np.array(a, dtype = float)
    dilution_times.append(a)
dilution_times = np.array(dilution_times) 
dilutions_flat = np.array(dilution_times[0])
for i in range(1, 4): 
    dilutions_flat = np.append(dilutions_flat, dilution_times[i])
dilutions_flat = np.sort(dilutions_flat)
dilutions_flat = np.delete(dilutions_flat, -14) #tips didn't attach 


### PLOT ### 
f3, ax3 = plt.subplots(1, 4, sharex=True, figsize = (12, 4))#, sharey = True)
concentrations = antibiotic 

colors = plt.cm.tab20b(np.linspace(0, 1, 2 * concentrations.size))

for k in range(4):
    i = 0
    for exp in experiments[k].exps:
        if k != 0: 
            if k >= 2:
                ax3[k-2].scatter(exp.times[exp.times < dilution_times[k][-1] + 8], exp.ods[exp.times < dilution_times[k][-1] + 8], label = str(exp.a0) + ' mg/L (' + str(i % 2 + 1) + ')', color = colors[i], s = 1)
                ax3[k-2].scatter(exp.times[exp.times >= dilution_times[k][-1] + 8], exp.ods[exp.times >= dilution_times[k][-1] + 8], label = 'evaporations,\nV < 20ul', color = 'grey', s = 1)
            else: 
                ax3[k+2].scatter(exp.times[exp.times < dilution_times[k][-1] + 8], exp.ods[exp.times < dilution_times[k][-1] + 8], label = str(exp.a0) + ' mg/L (' + str(i % 2 + 1) + ')', color = colors[i], s = 1)
                ax3[k+2].scatter(exp.times[exp.times >= dilution_times[k][-1] + 8], exp.ods[exp.times >= dilution_times[k][-1] + 8], label = 'evaporations,\nV < 20ul', color = 'grey', s = 1)           
        else: 
            ax3[k+2].scatter(exp.times[exp.times <= 8], exp.ods[exp.times <= 8],  label = str(exp.a0) + ' mg/L (' + str(i % 2 + 1) + ')', color = colors[i], s = 1)
            ax3[k+2].scatter(exp.times[np.logical_and(exp.times > 8, exp.times < 10)], exp.ods[np.logical_and(exp.times > 8, exp.times < 10)],  label = 'evaporations,\nV < 20ul', color = 'grey', alpha = 0.6, s = 1)
            ax3[k+2].scatter(exp.times[exp.times >= 10], exp.ods[exp.times >= 10],  
                           label = str(exp.a0) + ' mg/L (' + str(i % 2 + 1) + ')', color = colors[i], s = 1)            
        i += 1

for k in range(4): 
        ods = data[:, 12 * (k + 4) + 1 : 12 * (k + 4) + 9] - od_empty
        median =np.median(ods, axis = 1)
        if k >= 2: 
            ax3[k - 2].plot(time, median, color = 'black', label = 'median', alpha = 0.6, linewidth = 1)
        else: 
            ax3[k + 2].plot(time, median, color = 'black', label = 'median', alpha = 0.6, linewidth = 1)
        
        
for i in range(dilution_times.shape[0]): 
    for j in range(len(dilution_times[i])):
        if dilution_times[i][j] in dilutions_flat:
            if i == 0 and j == 0: 
                ax3[i + 2].axvline(x = dilution_times[i][j], alpha = 0.5, linestyle = '--', color = 'blue', label = 'fresh media', linewidth = 1) 
            elif i >= 2: 
                ax3[i - 2].axvline(x = dilution_times[i][j], alpha = 0.5, linestyle = '--', color = 'grey', label = 'dilution', linewidth = 1) 
            else: 
                ax3[i + 2].axvline(x = dilution_times[i][j], alpha = 0.5, linestyle = '--', color = 'grey', label = 'dilution', linewidth = 1) 

for i in range(ax3.size): 
    ax3[i].set_yscale('log')
    ax3[i].set_ylim(5*10**(-4), 1.2)
    ax3[i].set_xlim(0, 24)
    ax3[i].tick_params(axis='both', which='both', labelsize=10, width = 0.5)
    ax3[i].set_ylabel('     $OD_{600}$', fontsize = 10, fontname="Arial", labelpad=-6)
    ax3[i].set_xlabel('Time (hrs)', fontsize = 10, fontname="Arial")
    if i >= 2:
        ax3[i].axhline(y = 0.1, linewidth = 0.5) 
    else: 
        ax3[i].axhline(y = 0.05, linewidth = 0.5)
    ax3[i].set_title(media[i], fontsize = 10, fontname="Arial") 
    for j in ['bottom', 'left', 'right', 'top']: 
        ax3[i].spines[j].set_linewidth(0.5)

handles, labels = ax3[0].get_legend_handles_labels()
for i in range(1, ax3.size): 
    handles1, labels1 = ax3[i].get_legend_handles_labels()
    handles = handles + handles1
    labels = labels + labels1
by_label = OrderedDict(zip(labels, handles))
ax3[-1].legend(by_label.values(), by_label.keys(), markerscale=2, fontsize = 10, bbox_to_anchor=(1.85, 1), loc = 'upper right')
# f3.subplots_adjust(left=0.1, bottom=0.2, right=0.86, top=0.9, wspace=0.5, hspace=0.1)

f3.tight_layout()
# plt.rcParams['svg.fonttype'] = 'none'
# f3.savefig('fig5b.svg', format='svg', dpi=500)
f3.savefig('plots/fig5b.pdf', format='pdf', dpi=500)
# f3.savefig('fig5b.png', format='png', dpi=500)

plt.show() 