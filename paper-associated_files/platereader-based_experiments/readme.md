# Platereader based applications 

## About

This part of repository hosts content related to the ReacSight strategy to connect pipetting robot OT-2 (api v1) with a plate reader (Tecan Spark) to do repeated antibiotic treatments. 

## Structure 

This folder contains two parts: 
- data_generation: script of the experiments 
- data_analysis: code that generated the figures 

### Data generation 

OD measurements are done by repeating many times a short 5-10 minute-long program: 

```
Repeat 2 times with interval of 5 min: 
    Shaking : 10s, 5mm amplitude, 390 rpm frequency 
    Wait : 5s 
    Absorbance: 600 nm 
    Wait : 20s 
    Shaking : 10s, 5mm amplitude, 390 rpm frequency
    Wait : 20s
    Wait : 20s
```

Connection between the experiment controlling computer (further controller) and the platereader is achieve by clicking software ( ```pyautogui ``` package)  to launch or stop a pre-defined program and if needed to open or close the plate holder. The positions of necessary buttons are specified in the beginning : 
 
```python 
pos_edit_worklist_plate_in = (710,60)
pos_edit_worklist_start = (480,60)
pos_edit_worklist_end = (2420,1330)
pos_edit_worklist_pause = (2320,1330)
pos_edit_worklist_stop = (2460,1330)
pos_edit_worklist_plate_out_exp = (330,1330) 
``` 

The results of Tecan Spark programm are stored locally on the controller computer. Therefore the data are accessed and analysed directly in the notebook of the experiment. 

The connection between controller and pipetting robot is done by using the OT2 Python API v1 and FlaskAPP interface. ```flaskr.py``` is similar to the one already presented in the general strategy and for bioreactors, but it has specific definition of labware corresponding to plate in an open Tecan Spark holder and specific function for performing dilutions and automatically dropping tips after the dilution to limit risk of contaminations. 


### Data analysis 

This folder contains scripts that generate figures. 