### About

This repository hosts content related to the *ReacSight* strategy to enhance bioreactor arrays for automated measurements and reactive experiment control.

The related paper is available on [bioRxiv](https://www.biorxiv.org/content/10.1101/2020.12.27.424467v2).

The repository has two main folders:

* [ReacSight strategy](ReacSight-strategy): Description of the *ReacSight* strategy (software and hardware)
* [paper code & data](paper-code-data): Content related to the paper case studies (plasmid sequences, code to generate figures from data)

### The ReacSight strategy

#### Software

Listing of *ReacSight* software [components](ReacSight-strategy/software):

* A full example of a *ReacSight* platform software to use as a template
     + corresponding hardware components: Chi.Bio bioreactor array, Opentrons OT-2 robot, Guava cytometer
     + code [here](ReacSight-strategy/software/platform-example-template)
* [Examples](ReacSight-strategy/software/instrument-control-examples) of `Python` instrument APIs and corresponding `Flask` interfaces
    + LED strips (WS2812b) control
    + Luminex Guava EasyCyte 14HT cytometer control
    + Opentrons OT-2 control
    + Chi.Bio bioreactor array control
    - [Cytometry data analysis](ReacSight-strategy/software/cytometry-data-analysis)
        + parsing (specific to Guava-generated `FCS3.0` files)
        + gating
        + deconvolution
    - [Model predictive control](ReacSight-strategy/software/model-predictive-control`)

Software components were successfully installed and executed on a variety of environments:

* Windows 10 and Windows 7 computers with a miniconda-managed Python 3.6+ distribution
* OT-2 robot internal computer native linux distribution
* Chi.Bio beaglebone native debian linux distribution

#### Hardware

* Hardware files (`hardware` folder)
    - Sampling head
    - Sampling plate holder
    - Waste funnel

#### Short video presentation

A short video presenting ReacSight can be found [here](reacsight_presentation_video.mp4).

### Paper code and data

* Plasmid [sequences](paper-code-data/plasmid-sequences)
* Bioreactor and cytometry data deposited on Zenodo [doi:10.5281/zenodo.4776009](https://doi.org/10.5281/zenodo.4776009)
* [Code](paper-code-data/code-data-to-figures) to generate figures from data for the paper case studies
